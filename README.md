<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## PinguinSys

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- NWidart Modules
- Laravel Telescope
- Laravel file-manager
- Recze modules: SimplePay, SzamlaAgent, OTP, Barion

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Installation

composer install \
npm run prod \
php artisan cache:clear \
php artisan modelCache:clear \
php artisan route:cache \
php artisan config:cache \
php artisan event:cache \
php artisan queue:restart

## Linking storage

php artisan storage:link

## Generate translations

php artisan generate:translations

## Running in development

npm run watch-poll

## Development Login
http://localhost/admin

recze01@gmail.com \
aA123456

## Building for production

npm run prod

## Deploy script for Laravel Forge

cd /home/forge/liquid.bigsys.eu \
php artisan down --message="System is updating. Check back later." --retry=60 \
git checkout -- . \
git pull origin develop \
composer install --no-interaction --prefer-dist --optimize-autoloader \
npm install \

( flock -w 10 9 || exit 1 \
    echo 'Restarting FPM...'; sudo -S service php7.2-fpm reload ) 9>/tmp/fpmlock

if [ -f artisan ]; then \
    php artisan migrate:fresh --force \
fi

npm run dev \
php artisan up \
php artisan modelCache:clear \
php artisan cache:clear \
php artisan config:cache \
php artisan view:cache \
php artisan route:cache \
php artisan queue:flush \
php artisan queue:restart

## Templating

Put .vue files to resources/js/frontend/components/pages

If you would like it to editable use:

extends: Editable

## Core elements

ForgetPass\
Login\
Register\
ResetPass

## Contributing

- **Réczi Zsolt**
- **Bodák Szabolcs**
- **Bugyinszki László**
- **Macskásy Kamilla**
- **Lévai Norbert**
- **Polgár József**

## Security Vulnerabilities

If you discover a security vulnerability within PinguinSys, please send an e-mail to Zsolt Reczi via [recze01@gmail.com](mailto:recze01@gmail.com). All security vulnerabilities will be promptly addressed.

