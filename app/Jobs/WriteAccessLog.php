<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WriteAccessLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $path;
    public $method;
    public $inputs;
    public $id;

    private $dontReport = [
        'password',
        'password_again'
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path, $method, $inputs, $id)
    {
        $this->path = $path;
        $this->method = $method;
        $this->inputs = $inputs;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $accessLog = new AccessLog();
        $accessLog->user_id = $this->id;
        $accessLog->route = $this->path;
        $accessLog->action = $this->method;
        $accessLog->extra = $this->handleRequest();
        $accessLog->save();
    }

    private function handleRequest()
    {
        $requests = [];
        foreach($this->inputs as $k => $r)
        {
            if (!in_array($k, $this->dontReport))
            {
                $requests[$k] = $r;
            }
        }

        return $requests;
    }
}
