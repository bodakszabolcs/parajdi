<?php

namespace App\Console;

use App\Console\Commands\BackupDatabase;
use App\Console\Commands\GenerateLogsTable;
use App\Jobs\ClearOldDatabaseBackups;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Webshop\Console\MNBSync;
use Nwidart\Modules\Facades\Module;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        BackupDatabase::class,
        GenerateLogsTable::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(BackupDatabase::class)->dailyAt('23:55')->evenInMaintenanceMode();

        if (config('telescope.enabled')) {
            $schedule->command('telescope:prune --hours=72')->daily()->evenInMaintenanceMode();
        }

        $schedule->job(new ClearOldDatabaseBackups)->dailyAt('01:00');
        $schedule->call(GenerateLogsTable::class)
            ->monthlyOn(28)->emailOutputOnFailure(config('app.superuser_email'));

        $ws = Module::find('Webshop');
        if ($ws->active) {
            $schedule->command('mnb:sync-daily')->dailyAt('10:00');
        }

        $schedule->command('generate:sitemap')->hourly()->evenInMaintenanceMode();

        // $schedule->command('inspire')com
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
