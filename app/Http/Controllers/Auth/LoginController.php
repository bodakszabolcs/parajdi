<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Modules\User\Entities\User;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       //$this->middleware('guest')->except('logout');
    }
    public function SocialSignup($provider)
    {
        // Socialite will pick response data automatic
        $user = Socialite::driver($provider)->stateless()->user();
        $localUser =User::where($provider,'=',$user->id)->first();
        if(!$localUser){
            $localUser = User::where('email','=',$user->email)->where('email','NOT LIKE','')->first();
            if(!$localUser){
                $localUser = new User();
                $localUser->{$provider} = $user->id;
                $localUser->name = $user->name;
                $localUser->email = $user->email;
                $localUser->email_verified_at = date('Y-m-d H:i:d');
                $localUser->save();
                $billing = new UserBilling();
                $billing->user_id = $localUser->id;
                $billing->save();
                $shipping = new UserShipping();
                $shipping->user_id = $localUser->id;
                $shipping->save();
            }else {
                $localUser->{$provider} = $user->id;
                $localUser->name = $user->name;
                $localUser->email = $user->email;
                $localUser->save();
            }

        }
        $user->tokens()->where('last_used_at','<',now()->add('- 3 days'))->delete();
        $token = $user->createToken('login-'.$user->email.'-'.date("YmdHi"))->plainTextToken;
        $user->token = $token;
        $user->filters = $user->filters()->get();
        Auth::login($user);
        return response()->json($user);
    }
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    protected function authenticated(Request $request, $user)
    {
        $user->tokens()->where('last_used_at','<',now()->add('- 3 days'))->delete();
        if ($user->hasVerifiedEmail()) {
            $token = $user->createToken('login-'.$user->email.'-'.date("YmdHi"))->plainTextToken;

            $user->token = $token;
            $user->filters = $user->filters()->get();

            Auth::login($user);
            return $user;
        }

        return $request->expectsJson()
            ? response()->json(['errors' => ['email' => [__('Your email address is not verified.')]]], 403)
            : Redirect::route($this->redirectTo ?: 'verification.notice');
    }
}
