<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Page\Entities\Page;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return admin view layout.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Admin(Request $request)
    {

        return view('layouts.app_admin');
    }

    /**
     * Return frontend view layout and setting up meta data.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Frontend(Request $request)
    {

        return Page::returnFrontendContent($request);
    }

    /**
     * Handle 404 page.
     * @param Request $request
     */
    public function _404(Request $request)
    {

        abort(404);
    }
}
