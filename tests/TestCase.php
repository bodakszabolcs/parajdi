<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /*
     * Testcases - Unit:
     * Create empty
     * Create with data
     * Check unique validation.
     */
}
