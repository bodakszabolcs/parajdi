const mix = require('laravel-mix')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Frontend Asset Management
 |--------------------------------------------------------------------------
 */

mix.js(['resources/js/frontend/application/app.js'], 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({ processCssUrls: false })

mix.styles([
    'public/frontend/css/css.css',
    'public/frontend/css/css-1.css',
    'public/frontend/css/all.min.css',
    'public/frontend/css/ionicons.min.css',
    'public/frontend/css/themify-icons.css',
    'public/frontend/css/linearicons.css',
    'public/frontend/css/laticon.css',
    'public/frontend/css/simple-line-icons.css',
    'public/frontend/css/owl.carousel.min.css',
    'public/frontend/css/owl.theme.default.min.css',
    'public/frontend/css/magnific-popup.css',
    'public/frontend/css/slick.css',
    'public/frontend/css/slick-theme.css',
    'public/frontend/css/style.css',
    'public/frontend/css/responsive.css',
    'public/frontend/css/main.css',
    'public/assets/vendors/general/select2/dist/css/select2.min.css'
], 'public/css/frontend.css').options({ processCssUrls: false })
mix.scripts([
    'public/frontend/js/script.js',
    'public/frontend/js/slick.min.js',
    'public/assets/vendors/general/toastr/build/toastr.min.js',
    'public/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js',
    'public/frontend/js/owl.carousel.min.js',
    'public/assets/vendors/custom/js/vendors/sweetalert2.init.js',
    'public/assets/vendors/general/select2/dist/js/select2.js'
], 'public/js/mixed.js')

mix.styles([
    'node_modules/ContentTools/build/content-tools.min.css',
    'resources/sass/extra.css'
], 'public/css/vendor.css')
    .options({ processCssUrls: false })

/*
 |--------------------------------------------------------------------------
 | Admin Asset Management
 |--------------------------------------------------------------------------
 */

mix.js(['resources/js/admin/application/app-admin.js'], 'public/js')
    .sass('resources/sass/app-admin.scss', 'public/css')
    .options({ processCssUrls: false })

if (mix.inProduction()) {
    mix.version()

    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    include: [
                        path.resolve('node_modules/laravel-file-manager'),
                        path.resolve('node_modules/engine.io-client')
                    ],
                    use: [{
                        loader: 'babel-loader',
                        options: Config.babel()
                    }]
                }
            ]
        }
    })
} else {
    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                }
            ]
        }
    })
}
