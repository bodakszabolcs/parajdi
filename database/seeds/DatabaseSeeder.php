<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\App::setLocale('hu');
        $this->call(UsersTableSeeder::class);
        $this->call(\Modules\Admin\Database\Seeders\AdminDatabaseSeeder::class);
        $this->call(\Modules\Blog\Database\Seeders\BlogDatabaseSeeder::class);
        $this->call(\Modules\Slider\Database\Seeders\SliderDatabaseSeeder::class);
        $this->call(\Modules\Category\Database\Seeders\CategoryDatabaseSeeder::class);
        $this->call(\Modules\Menu\Database\Seeders\MenuDatabaseSeeder::class);
        $this->call(\Modules\ModuleBuilder\Database\Seeders\ModuleBuilderDatabaseSeeder::class);
        $this->call(\Modules\Page\Database\Seeders\PageDatabaseSeeder::class);
        $this->call(\Modules\Role\Database\Seeders\RoleDatabaseSeeder::class);
        $this->call(\Modules\System\Database\Seeders\SystemDatabaseSeeder::class);
        $this->call(\Modules\Translation\Database\Seeders\TranslationDatabaseSeeder::class);
        $this->call(\Modules\User\Database\Seeders\UserDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\WebshopFrontendDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\MenusTableSeeder::class);
      //  $this->call(\Modules\WebshopFrontend\Database\Seeders\MenuItemsTableSeeder::class);
        $this->call(\Modules\Color\Database\Seeders\ColorDatabaseSeeder::class);
        $this->call(\Modules\Pack\Database\Seeders\PackDatabaseSeeder::class);
        $this->call(\Modules\Type\Database\Seeders\TypeDatabaseSeeder::class);
        $this->call(\Modules\Product\Database\Seeders\ProductDatabaseSeeder::class);
        $this->call(\Modules\ProductVariation\Database\Seeders\ProductVariationDatabaseSeeder::class);
        $this->call(\Modules\ProductQuestion\Database\Seeders\ProductQuestionDatabaseSeeder::class);
        $this->call(\Modules\ProductRating\Database\Seeders\ProductRatingDatabaseSeeder::class);
        $this->call(\Modules\ProductCategory\Database\Seeders\ProductCategoryDatabaseSeeder::class);

$this->call(\Modules\OrderItem\Database\Seeders\OrderItemDatabaseSeeder::class);

$this->call(\Modules\Banner\Database\Seeders\BannerDatabaseSeeder::class);

$this->call(\Modules\Order\Database\Seeders\OrderDatabaseSeeder::class);

$this->call(\Modules\Order\Database\Seeders\OrderDatabaseSeeder::class);

$this->call(\Modules\Coupon\Database\Seeders\CouponDatabaseSeeder::class);

/* ModuleBuilderSeedArea *//* ModuleBuilderSeedAreaEnd */

        if (config('app.env') == 'local') {
            \Modules\User\Entities\User::unsetEventDispatcher();
            \Modules\Role\Entities\Role::unsetEventDispatcher();
            \Modules\Blog\Entities\Blog::unsetEventDispatcher();
            \Modules\Page\Entities\Page::unsetEventDispatcher();
            \Modules\Color\Entities\Color::unsetEventDispatcher();
            \Modules\Pack\Entities\Pack::unsetEventDispatcher();
            \Modules\Type\Entities\Type::unsetEventDispatcher();
            \Modules\Product\Entities\Product::unsetEventDispatcher();
            \Modules\ProductVariation\Entities\ProductVariation::unsetEventDispatcher();
            \Modules\ProductCategory\Entities\ProductCategory::unsetEventDispatcher();

\Modules\OrderItem\Entities\OrderItem::unsetEventDispatcher();

\Modules\Banner\Entities\Banner::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Coupon\Entities\Coupon::unsetEventDispatcher();

/* EventDispatcherArea *//* EventDispatcherAreaEnd */

            $user = factory(\Modules\User\Entities\User::class, 20)->create()
                ->each(function ($user) {
                    $user->shipping()->save(factory(\Modules\User\Entities\UserShipping::class)->make([
                        'user_id' => $user->id
                    ]));

                    $user->billing()->save(factory(\Modules\User\Entities\UserBilling::class)->make([
                        'user_id' => $user->id
                    ]));

    });

            $role = factory(\Modules\Role\Entities\Role::class, 8)->create();

            $blog = factory(\Modules\Blog\Entities\Blog::class, 20)->create()
                ->each(function ($b) {
                    $b->categories()->save(factory(\Modules\Category\Entities\Category::class)->make());
                    $b->tags()->save(factory(\Modules\Category\Entities\Tag::class)->make());
                });

            $model = factory(\Modules\Color\Entities\Color::class, 5)->create();
            $model = factory(\Modules\Slider\Entities\Slider::class, 5)->create();

            $model = factory(\Modules\Pack\Entities\Pack::class, 5)->create();

            $model = factory(\Modules\Type\Entities\Type::class, 5)->create();
            $model = factory(\Modules\ProductCategory\Entities\ProductCategory::class, 20)->create();
            foreach (\Modules\ProductCategory\Entities\ProductCategory::limit(10)->offset(10)->get()  as $c){
                $c->parent_id = rand(1,10);
                $c->save();
            }

            $model = factory(\Modules\Product\Entities\Product::class, 300)->create();
            foreach (\Modules\Product\Entities\Product::all() as $p){
                $p->categories()->sync(rand(1,20));
            }
            $model = factory(\Modules\ProductQuestion\Entities\ProductQuestion::class, 300)->create();
            $model = factory(\Modules\ProductRating\Entities\ProductRating::class, 300)->create();

            $model = factory(\Modules\ProductVariation\Entities\ProductVariation::class, 900)->create();
            foreach (\Modules\ProductVariation\Entities\ProductVariation::all() as $v){
                for($i = 0; $i< 5;$i++) {
                    $k = new \Modules\ProductVariation\Entities\ProductVariationImage();
                    $k->variation_id = $v->id;
                    $k->url = 'https://source.unsplash.com/800x800/?salt&'.rand(0,9999);
                    $k->save();
                }
            }


            /*$model = factory(\Modules\OrderItem\Entities\OrderItem::class, 5)->create();*/

            $model = factory(\Modules\Banner\Entities\Banner::class, 6)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Coupon\Entities\Coupon::class, 5)->create();

/* FakerArea *//* FakerAreaEnd */

        }
    }
}
