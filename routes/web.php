<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::any('/'.config('app.admin_url').'/{any?}', 'Controller@Admin')->where('any', '.*');
Route::any('/{any?}', 'Controller@Frontend')->where('any', '.*');

