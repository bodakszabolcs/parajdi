export const API_URL = process.env.MIX_APP_URL + '/' + process.env.MIX_API_VERSION
export default API_URL
