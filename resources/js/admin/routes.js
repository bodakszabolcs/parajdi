import Admin from '../../../Modules/Admin/Resources/assets/js/components/Admin'
import NotFound from '../../../Modules/Admin/Resources/assets/js/components/NotFound'

const mixedroutes = []

/* ROUTE CONFIG START */
const routesAdmin = require('./../../../Modules/Admin/Resources/assets/js/router/index')
const routesSlider = require('./../../../Modules/Slider/Resources/assets/js/router/index')
const routesTranslation = require('./../../../Modules/Translation/Resources/assets/js/router/index')
const routesSystem = require('./../../../Modules/System/Resources/assets/js/router/index')
const routesUsers = require('./../../../Modules/User/Resources/assets/js/router/index')
const routesRoles = require('./../../../Modules/Role/Resources/assets/js/router/index')
const routesModuleBuilder = require('./../../../Modules/ModuleBuilder/Resources/assets/js/router/index')
const routesPage = require('./../../../Modules/Page/Resources/assets/js/router/index')
const routesMenu = require('./../../../Modules/Menu/Resources/assets/js/router/index')
const routesBlog = require('./../../../Modules/Blog/Resources/assets/js/router/index')
const routesCategory = require('./../../../Modules/Category/Resources/assets/js/router/index')
const routesColor = require('./../../../Modules/Color/Resources/assets/js/router/index')
const routesPack = require('./../../../Modules/Pack/Resources/assets/js/router/index')
const routesType = require('./../../../Modules/Type/Resources/assets/js/router/index')
const routesProduct = require('./../../../Modules/Product/Resources/assets/js/router/index')
const routesProductVariation = require('./../../../Modules/ProductVariation/Resources/assets/js/router/index')
const routesProductCategory = require('./../../../Modules/ProductCategory/Resources/assets/js/router/index')
const routesProductRating = require('./../../../Modules/ProductRating/Resources/assets/js/router/index')
const routesProductQuestion = require('./../../../Modules/ProductQuestion/Resources/assets/js/router/index')
const routesOrderItem = require('../../../Modules/OrderItem/Resources/assets/js/router/index')
const routesBanner = require('./../../../Modules/Banner/Resources/assets/js/router/index')
const routesOrder = require('./../../../Modules/Order/Resources/assets/js/router/index')
const routesCoupon = require('./../../../Modules/Coupon/Resources/assets/js/router/index')
/* ROUTE CONFIG END */

/* ROUTE ARRAY START */
const routes = [routesSystem.default, routesTranslation.default, routesAdmin.default, routesUsers.default, routesRoles.default, routesModuleBuilder.default, routesPage.default, routesMenu.default, routesBlog.default, routesCategory.default, routesColor.default, routesPack.default, routesType.default, routesProduct.default, routesProductVariation.default, routesProductCategory.default, routesSlider.default, routesProductRating.default, routesProductQuestion.default, routesOrderItem.default, routesBanner.default, routesOrder.default, routesCoupon.default] // routesEnd
/* ROUTE ARRAY END */
for (const it in routes) {
    for (const elem in routes[it]) {
        let have = false
        for (const find in mixedroutes) {
            if (mixedroutes[find].path === routes[it][elem].path) {
                have = true
                mixedroutes[find].children = mixedroutes[find].children.concat(routes[it][elem].children)
            }
        }
        if (!have) {
            mixedroutes.push(routes[it][elem])
        }
    }
}
mixedroutes.push(
    {
        path: '/admin',
        component: Admin,
        meta: {
            title: 'Admin',
            requiresAuth: true
        },
        children: [{
            path: '*',
            component: NotFound,
            meta: {
                title: 'A keresett oldal nem található'
            }
        }
        ]
    })
export default mixedroutes
