import Vue from 'vue'
import Vuex from 'vuex'
import defaultStore from './modules/defaultStore'
import htmlClass from './modules/htmlclass.module'
import config from './modules/config.module'
import breadcrumbs from './modules/breadcrumbs.module'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        htmlClass,
        config,
        breadcrumbs,
        defaultStore
    }
})
