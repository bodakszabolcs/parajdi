export default ({
    namespaced: true,
    state: {
        loading: true,
        errors: [],
        pageLanguage: process.env.MIX_APP_DEFAULT_LOCALE,
        languages: {
            hu: {},
            en: {}
        },
        pageCurrency: '1',
        model: {},
        prefixes: {},
        loggedIn: false,
        user: {}
    },
    mutations: {
        change (state, payload) {
            // mutate state
            state.loading = payload
        },
        setLanguage (state, payload) {
            state.languages[payload.lang] = payload.payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setModel (state, payload) {
            state.model = payload
        },
        setLoggedIn (state, payload) {
            state.loggedIn = payload
        },
        setUser (state, payload) {
            state.user = payload
        },
        setPageLanguage (state, payload) {
            state.pageLanguage = payload
        },
        setPageCurrency (state, payload) {
            state.pageCurrency = payload
        },
        setPrefixes (state, payload) {
            state.prefixes = payload
        }
    },
    getters: {}
})
