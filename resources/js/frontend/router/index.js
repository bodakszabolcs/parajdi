import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../components/Layout'
import Static from '../components/Static'
import NotFound from '../components/NotFound'
import store from '../../store/index'
import axios from 'axios'
import Post from '../components/Post'
import Category from '../components/Category'
import Webshop from '../../../../Modules/WebshopFrontend/Resources/assets/js/components/Webshop'
import ProductDetails
    from '../../../../Modules/WebshopFrontend/Resources/assets/js/components/product-details/ProductDetails'

const routes = require('./routes')
const routesFrontend = require('../routes-frontend')

Vue.use(VueRouter)

const childrenRoutes = []
const wsChildrenRoutes = []

if (routes.default.length > 0) {
    for (const it in routes.default) {
        childrenRoutes.push(routes.default[it])
    }
}

if (routesFrontend.default.length > 0) {
    for (const it in routesFrontend.default) {
        wsChildrenRoutes.push(routesFrontend.default[it])
    }
}

childrenRoutes.push({
    path: '404',
    name: 'NotFound',
    component: NotFound
})

childrenRoutes.push({
    path: '/',
    component: Static
})

const definedRoutes = [
    {
        path: '/',
        component: Layout,
        children: childrenRoutes
    }
]

const router = new VueRouter({
    mode: 'history',
    routes: definedRoutes
})

axios.get(`${process.env.MIX_APP_URL}/storage/prefixes.json`).then(response => {
    wsChildrenRoutes.push({
        path: `${response.data.product}/:slug`,
        name: 'ProductDetails',
        component: ProductDetails,
        meta: {
            title: 'ProductDetails'
        }
    })
    router.addRoutes([
        {
            path: '/',
            component: Layout,
            children: [
                {
                    name: 'Post',
                    path: `${response.data.blog}/:slug`,
                    component: Post
                },
                {
                    name: 'Category',
                    path: `${response.data.category}`,
                    component: Category
                },
                {
                    name: 'Webshop',
                    path: `${response.data.shop}`,
                    component: Webshop,
                    children: wsChildrenRoutes
                },
                {
                    path: '*',
                    component: Static
                }
            ]
        }
    ])
    store.commit('defaultStore/setPrefixes', response.data)
})

router.beforeEach((to, from, next) => {
    // eslint-disable-next-line no-undef
    store.commit('defaultStore/setErrors', {})
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if ($cookies.get('token') === null) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router
