const mixedroutes = []

/* ROUTE CONFIG START */
const routesWebshopFrontend = require('./../../../Modules/WebshopFrontend/Resources/assets/js/router/index')
/* ROUTE CONFIG END */

/* ROUTE ARRAY START */
const routes = [routesWebshopFrontend.default] // routesEnd
/* ROUTE ARRAY END */
for (const it in routes) {
    for (const elem in routes[it]) {
        let have = false
        for (const find in mixedroutes) {
            if (mixedroutes[find].path === routes[it][elem].path) {
                have = true
                mixedroutes[find].children = mixedroutes[find].children.concat(routes[it][elem].children)
            }
        }
        if (!have) {
            mixedroutes.push(routes[it][elem])
        }
    }
}

export default mixedroutes
