<title>{{ config('app.name', 'Laravel') }}@if($title){{' - ' . $title}}@endif</title>
<meta name="description" content="{{$description}}" />

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{$title}}">
<meta itemprop="description" content="{{$description}}">
<meta itemprop="image" content="{{$meta_image}}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{{$title}}">
<meta name="twitter:description" content="{{$description}}">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="{{$meta_image}}">

<!-- Open Graph data -->
<meta property="og:title" content="{{$title}}" />
<meta property="og:type" content="{{$type or 'article'}}" />
<meta property="og:url" content="{{$url or url('/')}}" />
<meta property="og:image" content="{{$meta_image}}" />
<meta property="og:image:width" content="{{$width or '0'}}" />
<meta property="og:image:height" content="{{$height or '0'}}" />
<meta property="og:description" content="{{$description}}" />
<meta property="og:site_name" content="{{ config('app.name', 'Laravel') }}" />
<meta property="article:published_time" content="{{$publish_date or date("Y-m-dTH:i:s")}}" />
<meta property="article:modified_time" content="{{$publish_date or date("Y-m-dTH:i:s")}}" />
<meta property="article:section" content="{{$section}}" />
<meta property="article:tag" content="{{$tags}}" />
