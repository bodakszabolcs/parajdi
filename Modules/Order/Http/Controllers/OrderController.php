<?php

namespace Modules\Order\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Order\Entities\Order;
use Illuminate\Http\Request;
use Modules\Order\Http\Requests\OrderCreateRequest;
use Modules\Order\Http\Requests\OrderUpdateRequest;
use Modules\Order\Transformers\OrderViewResource;
use Modules\Order\Transformers\OrderListResource;

class OrderController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Order();
        $this->viewResource = OrderViewResource::class;
        $this->listResource = OrderListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = OrderCreateRequest::class;
        $this->updateRequest = OrderUpdateRequest::class;
    }

}
