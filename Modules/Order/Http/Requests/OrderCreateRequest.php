<?php

namespace Modules\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_total' => 'required',
			'shpping_price' => 'required',
			'discount' => 'required',
			'coupon' => 'required',
			'status' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'uuid' => __('uuid'),
'order_total' => __('Order total'),
'shpping_price' => __('Shipping price'),
'discount' => __('Discount'),
'coupon' => __('Coupon code'),
'status' => __('Status'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
