<?php

namespace Modules\Order\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class OrderListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "uuid" => $this->uuid,
		    "order_total" => $this->order_total,
		    "shpping_price" => $this->shpping_price,
		    "discount" => $this->discount,
		    "coupon" => $this->coupon,
		    "status" => $this->status,
		     ];
    }
}
