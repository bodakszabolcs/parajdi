import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Order from '../components/Order'
import OrderList from '../components/OrderList'
import OrderCreate from '../components/OrderCreate'
import OrderEdit from '../components/OrderEdit'
export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'orders',
                component: Order,
                meta: {
                    title: 'Orders'
                },
                children: [
                    {
                        path: 'index',
                        name: 'OrderList',
                        component: OrderList,
                        meta: {
                            title: 'Orders',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/orders/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/orders/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'OrderCreate',
                        component: OrderCreate,
                        meta: {
                            title: 'Create Orders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/orders/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrderEdit',
                        component: OrderEdit,
                        meta: {
                            title: 'Edit Orders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/orders/index'
                        }
                    }
                ]
            }
        ]
    }
]
