<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    class CreateOrderTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::dropIfExists('orders');
            Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->text("uuid")->nullable();
                $table->bigInteger("user_id")->nullable();
                $table->float("order_total")->nullable();
                $table->float("shipping_price")->nullable();
                $table->float("discount")->nullable();
                $table->string("coupon")->nullable();
                $table->integer("payment_method_id")->nullable();
                $table->integer("payment_status")->nullable();
                $table->text("payment_info")->nullable();
                $table->tinyInteger("status")->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(["deleted_at"]);

            });
            Schema::dropIfExists('order_details');
            Schema::create('order_details', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->bigInteger("order_id")->nullable();
                $table->string('shipping_name')->nullable();
                $table->string('shipping_email')->nullable();
                $table->string('shipping_phone')->nullable();
                $table->bigInteger('shipping_country_id')->nullable()->unsigned();
                $table->bigInteger('shipping_city_id')->nullable();
                $table->string('shipping_zip')->nullable();
                $table->text('shipping_address')->nullable();
                $table->text('shipping_note')->nullable();
                $table->string('billing_name')->nullable();
                $table->string('billing_company_name')->nullable();
                $table->string('billing_email')->nullable();
                $table->string('billing_phone')->nullable();
                $table->string('billing_country')->nullable();
                $table->string('billing_city')->nullable();
                $table->string('billing_zip')->nullable();
                $table->text('billing_address')->nullable();
                $table->string('vat_number')->nullable();

                $table->timestamps();

            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('orders');
        }
    }
