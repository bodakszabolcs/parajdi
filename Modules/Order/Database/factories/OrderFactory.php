<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Order\Entities\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        "uuid" => $faker->realText(),
        "user_id" => 1,
"order_total" => rand(1000,5000),
"shipping_price" => rand(1000,5000),
"discount" => rand(1000,5000),
"coupon" => $faker->realText(),
"status" => rand(1,10),

    ];
});
