<?php

namespace Modules\Order\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Order\Entities\Order;
use Modules\Order\Observers\OrderObserver;

class OrderServiceProvider extends ModuleServiceProvider
{
    protected $module = 'order';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Order::observe(OrderObserver::class);
    }
}
