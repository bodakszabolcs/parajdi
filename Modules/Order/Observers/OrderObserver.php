<?php

namespace Modules\Order\Observers;

use Modules\Order\Entities\Order;

class OrderObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function saved(Order $model)
    {
        Order::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function created(Order $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function updated(Order $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function deleted(Order $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function restored(Order $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Order\Entities\Order  $model
     * @return void
     */
    public function forceDeleted(Order $model)
    {
        //
    }
}
