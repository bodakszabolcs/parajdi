<?php

namespace Modules\Order\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Cities extends BaseModel
{
    protected $table = 'cities';
    protected $fillable = [
      'id',
      'name',

    ];
}
