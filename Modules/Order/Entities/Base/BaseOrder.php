<?php

namespace Modules\Order\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Coupon\Entities\Coupon;
use Spatie\Translatable\HasTranslations;
use Modules\User\Entities\User;


abstract class BaseOrder extends BaseModel
{


    protected $table = 'orders';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'order_total',
                    'title' => 'Order total',
                    'type' => 'text',
                    ],[
                    'name' => 'shpping_price',
                    'title' => 'Shipping price',
                    'type' => 'text',
                    ],[
                    'name' => 'discount',
                    'title' => 'Discount',
                    'type' => 'text',
                    ],[
                    'name' => 'coupon',
                    'title' => 'Coupon code',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = ["user"];

    protected $fillable = ['shpping_price','discount','coupon','status'];

    protected $casts = [];

     public function user()
    {
        return $this->hasOne('Modules\User\Entities\User','id','user_id');
    }
    public function coupons()
    {
        return Coupon::where('code','=',$this->coupon)->first();
    }
    public function items()
    {
        return $this->hasMany('Modules\OrderItem\Entities\OrderItem','order_id','id');
    }



}
