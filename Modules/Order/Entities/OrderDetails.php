<?php

namespace Modules\Order\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class OrderDetails extends BaseModel
{
    use Cachable;
    protected $table = 'order_details';
    protected $dates = ['created_at','updated_at'];
    protected $fillable = [
      'shipping_name',
      'shipping_email',
      'shipping_phone',
      'shipping_country_id',
      'shipping_city_id',
      'shipping_zip',
      'shipping_address',
      'shipping_note',
      'billing_name',
      'billing_email',
      'billing_phone',
      'billing_country',
      'billing_city',
      'billing_zip',
      'billing_address',
      'billing_company_name',
      'vat_number',

    ];
}
