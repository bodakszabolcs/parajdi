<?php

return [
    'name' => 'Order',

                 'menu_order' => 24,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Order',

                      'route' =>'/'.env('ADMIN_URL').'/orders/index',

                     ]

                 ]
];
