<?php

namespace Modules\Coupon\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CouponListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "code" => $this->code,
		    "date_from" => $this->date_from,
		    "date_to" => $this->date_to,
		    "type" => $this->type,
		    "value" => $this->value,
		    "users" => $this->users,
		     ];
    }
}
