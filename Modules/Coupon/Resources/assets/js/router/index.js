import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Coupon from '../components/Coupon'
import CouponList from '../components/CouponList'
import CouponCreate from '../components/CouponCreate'
import CouponEdit from '../components/CouponEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'coupon',
                component: Coupon,
                meta: {
                    title: 'Coupons'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CouponList',
                        component: CouponList,
                        meta: {
                            title: 'Coupons',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/coupon/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/coupon/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CouponCreate',
                        component: CouponCreate,
                        meta: {
                            title: 'Create Coupons',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/coupon/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CouponEdit',
                        component: CouponEdit,
                        meta: {
                            title: 'Edit Coupons',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/coupon/index'
                        }
                    }
                ]
            }
        ]
    }
]
