<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Coupon\Entities\Coupon;

$factory->define(Coupon::class, function (Faker $faker) {
    return [
        "code" => $faker->realText(),
"date_from" => $faker->dateTime(),
"date_to" => $faker->dateTime(),
"type" => rand(1,10),
"value" => rand(1000,5000),
"users" => $faker->realText(),

    ];
});
