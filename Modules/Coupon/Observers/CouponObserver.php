<?php

namespace Modules\Coupon\Observers;

use Modules\Coupon\Entities\Coupon;

class CouponObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function saved(Coupon $model)
    {
        Coupon::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function created(Coupon $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function updated(Coupon $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function deleted(Coupon $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function restored(Coupon $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Coupon\Entities\Coupon  $model
     * @return void
     */
    public function forceDeleted(Coupon $model)
    {
        //
    }
}
