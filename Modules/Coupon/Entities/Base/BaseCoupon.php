<?php

namespace Modules\Coupon\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Coupon\Entities\Coupon;
use Spatie\Translatable\HasTranslations;


abstract class BaseCoupon extends BaseModel
{


    protected $table = 'coupons';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'code',
                    'title' => 'Code',
                    'type' => 'text',
                    ],[
                    'name' => 'date_from',
                    'title' => 'Valid from',
                    'type' => 'text',
                    ],[
                    'name' => 'date_to',
                    'title' => 'Valid to',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }
    public function generateCouponCode(){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 7; $i++) {
            $randstring .= $characters[rand(0, strlen($characters))];
        }
        $c =Coupon::where('code','=',$randstring)->first();
        if(is_null($c)) {
            return $randstring;
        }
        return  $this->generateCouponCode();
    }
    public function fillAndSave(array $request)
    {
        $this->fill($request);
        if(empty($this->code)){
            $this->code = $this->generateCouponCode();
        }
        $this->save();

        return $this;
    }
    protected $with = [];

    protected $fillable = ['code','date_from','date_to','type','value','users'];

    protected $casts = ['users' => 'array',];


}
