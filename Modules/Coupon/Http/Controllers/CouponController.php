<?php

namespace Modules\Coupon\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Coupon\Entities\Coupon;
use Illuminate\Http\Request;
use Modules\Coupon\Http\Requests\CouponCreateRequest;
use Modules\Coupon\Http\Requests\CouponUpdateRequest;
use Modules\Coupon\Transformers\CouponViewResource;
use Modules\Coupon\Transformers\CouponListResource;

class CouponController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Coupon();
        $this->viewResource = CouponViewResource::class;
        $this->listResource = CouponListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CouponCreateRequest::class;
        $this->updateRequest = CouponUpdateRequest::class;
    }
}
