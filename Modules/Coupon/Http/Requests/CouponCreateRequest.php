<?php

    namespace Modules\Coupon\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    class CouponCreateRequest extends FormRequest
    {
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'date_from' => 'required', 'date_to' => 'required', 'type' => 'required',
                'value' => 'required_if:type,3',
            ];
        }

        public function attributes()
        {
            return [
                'code' => __('Code'), 'date_from' => __('Valid from'), 'date_to' => __('Valid to'),
                'type' => __('Type'), 'value' => __('Price or Discount percent'), 'users' => __('User list'),
            ];
        }

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    }
