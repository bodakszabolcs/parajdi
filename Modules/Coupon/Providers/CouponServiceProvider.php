<?php

namespace Modules\Coupon\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Coupon\Entities\Coupon;
use Modules\Coupon\Observers\CouponObserver;

class CouponServiceProvider extends ModuleServiceProvider
{
    protected $module = 'coupon';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Coupon::observe(CouponObserver::class);
    }
}
