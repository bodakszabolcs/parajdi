<?php

return [
    'name' => 'Coupon',

                 'menu_order' => 24,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Coupon',

                      'route' =>'/'.env('ADMIN_URL').'/coupon/index',

                     ]

                 ]
];
