<?php

namespace Modules\Product\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\ProductQuestion\Entities\ProductQuestion;
use Modules\ProductRating\Entities\ProductRating;
use Modules\ProductVariation\Entities\ProductVariation;
use Modules\ProductVariation\Entities\ProductVariationImage;
use Spatie\Translatable\HasTranslations;


abstract class BaseProduct extends BaseModel
{


    protected $table = 'products';
    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public function variations(){

        return $this->hasMany('Modules\ProductVariation\Entities\ProductVariation', 'product_id', 'id');
    }
    public function categories()
    {
        return $this->belongsToMany('Modules\ProductCategory\Entities\ProductCategory', 'product_categories', 'product_id', 'category_id');
    }
    public function getQuestions(){
        return ProductQuestion::where('moderated','=',1)->where('product_id','=',$this->id)->orderBy('created_at','desc')->get();
    }
    public function getRatings(){
        return ProductRating::where('moderated','=',1)->where('product_id','=',$this->id)->orderBy('created_at','desc')->get();
    }
    public function getRatingTable(){
        return ProductRating::where('moderated','=',1)->select('rate',\DB::raw('count(*) as cnt'))->where('product_id','=',$this->id)->groupBy('rate')->get();
    }
    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Product name',
                    'type' => 'text',
                    ],[
                    'name' => 'slug',
                    'title' => 'slug',
                    'type' => 'text',
                    ],[
                    'name' => 'meta_title',
                    'title' => 'Meta title',
                    'type' => 'text',
                    ],[
                    'name' => 'meta_description',
                    'title' => 'Meta description',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','slug','description','meta_title','meta_description'];

    protected $casts = [];

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();
        $cats = [];
        foreach ($request['categories'] as $k =>$v){
            if($v){
                $cats[$k] = $k;
            }
        }
        $this->categories()->sync($cats);
        $variations = $request['variations'];
        $vIds = [];
        foreach ($variations as $v){
            $variation = ProductVariation::where('id','=',Arr::get($v,'id',null))->first();
            if(is_null($variation)){
                $variation = new ProductVariation();
                $variation->product_id = $this->id;
                $variation->save();
            }
            if(empty($variation->sku)) {
                $variation->sku = 'PR' . str_pad($this->id, 0, '0', STR_PAD_LEFT) . '-' . str_pad($variation->id, 0, '0', STR_PAD_LEFT);
            }
            $variation->fill($v);
            $variation->save();
            $vIds[$variation->id]= $variation->id;
            //Variation Images
            $vImages = $v['images'];
            $iIds= [];
            foreach ($vImages as $i){
                $prodImages = ProductVariationImage::where('id','=',Arr::get($i,'id',null))->first();
                if(is_null($prodImages)){
                    $prodImages = new ProductVariationImage();

                }
                $prodImages->variation_id = $variation->id;
                $prodImages->url = $i['url'];
                $prodImages->save();
                $iIds[$prodImages->id] =$prodImages->id;
            }
            ProductVariationImage::where('variation_id','=',$variation->id)->whereNotIn('id',$iIds)->delete();

        }
        ProductVariation::where('product_id','=',$this->id)->whereNotIn('id',$vIds)->forceDelete();
        $this->save();

        return $this;
    }
}
