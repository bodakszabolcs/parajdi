<?php

namespace Modules\Product\Entities;

use Modules\Product\Entities\Base\BaseProduct;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Product extends BaseProduct
{
    use SoftDeletes, Cachable, HasTranslations;


    public $translatable = ['name','description','meta_title','meta_description'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
