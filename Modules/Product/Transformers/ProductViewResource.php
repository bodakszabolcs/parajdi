<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Category\Entities\Category;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Color\Entities\Color;
use Modules\Pack\Entities\Pack;
use Modules\ProductCategory\Entities\ProductCategory;
use Modules\ProductVariation\Transformers\ProductVariationViewResource;
use Modules\Type\Entities\Type;
class ProductViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->getTranslatable('name'),
		    "slug" => $this->slug,
		    "description" => $this->getTranslatable('description'),
		    "meta_title" => $this->getTranslatable('meta_title'),
		    "meta_description" => $this->getTranslatable('meta_description'),
            "variations" => ProductVariationViewResource::collection($this->variations()->get()),
            "categories" => $this->categories()->pluck('product_categories.id','product_categories.id'),
            "selectables" => [
                "colors" => Color::all()->pluck('name','id'),
                "packs" => Pack::all()->pluck('name','id'),
                "types" => Type::all()->pluck('name','id'),
                "categories" => CategoryTreeResource::collection(ProductCategory::all())
            ]
		     ];
    }
}
