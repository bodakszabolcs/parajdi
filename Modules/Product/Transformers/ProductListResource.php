<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProductListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "slug" => $this->slug,
		    "description" => $this->description,
		    "meta_title" => $this->meta_title,
		    "meta_description" => $this->meta_description,
		     ];
    }
}
