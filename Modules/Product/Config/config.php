<?php

return [
    'name' => 'Product',

                 'menu_order' => 16,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Product',

                      'route' =>'/'.env('ADMIN_URL').'/product/index',

                     ]

                 ]
];
