<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Product\Entities\Product;

$factory->define(Product::class, function (Faker $faker) {
    $name =$faker->unique()->text(20);
    return [
        "name" => $name,
        "description" => $faker->realText(),
        "meta_title" => $faker->realText(),
        "meta_description" => $faker->realText(),
        "slug"=> \Illuminate\Support\Str::slug($name)
    ];
});
