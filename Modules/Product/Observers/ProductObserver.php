<?php

namespace Modules\Product\Observers;

use Modules\Product\Entities\Product;

class ProductObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function saved(Product $model)
    {
        Product::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function created(Product $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function updated(Product $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function deleted(Product $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function restored(Product $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Product\Entities\Product  $model
     * @return void
     */
    public function forceDeleted(Product $model)
    {
        //
    }
}
