import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Product from '../components/Product'
import ProductList from '../components/ProductList'
import ProductCreate from '../components/ProductCreate'
import ProductEdit from '../components/ProductEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'product',
                component: Product,
                meta: {
                    title: 'Products'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProductList',
                        component: ProductList,
                        meta: {
                            title: 'Products',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/product/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProductCreate',
                        component: ProductCreate,
                        meta: {
                            title: 'Create Products',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProductEdit',
                        component: ProductEdit,
                        meta: {
                            title: 'Edit Products',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product/index'
                        }
                    }
                ]
            }
        ]
    }
]
