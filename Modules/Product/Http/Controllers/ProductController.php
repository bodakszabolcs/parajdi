<?php

namespace Modules\Product\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Product\Entities\Product;
use Illuminate\Http\Request;
use Modules\Product\Http\Requests\ProductCreateRequest;
use Modules\Product\Http\Requests\ProductUpdateRequest;
use Modules\Product\Transformers\ProductViewResource;
use Modules\Product\Transformers\ProductListResource;

class ProductController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Product();
        $this->viewResource = ProductViewResource::class;
        $this->listResource = ProductListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProductCreateRequest::class;
        $this->updateRequest = ProductUpdateRequest::class;
    }

}
