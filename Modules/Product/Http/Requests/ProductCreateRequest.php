<?php

    namespace Modules\Product\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    class ProductCreateRequest extends FormRequest
    {
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'name' => 'required', 'description' => 'required', 'meta_title' => 'required',
                'meta_description' => 'required',
                'categories' => 'required',
                'variations.*.weight'=>'required|not_in:0',
                'variations.*.in_stock'=>'required',
                'variations.*.price'=>'required|not_in:0'
            ];
        }

        public function attributes()
        {
            return [
                'name' => __('Product name'), 'slug' => __('slug'), 'description' => __('Description'),
                'meta_title' => __('Meta title'), 'meta_description' => __('Meta description'),
                'variations.*.weight' => __('Variation weight'),
                'variations.*.price' => __('Variation price ')

            ];
        }

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    }
