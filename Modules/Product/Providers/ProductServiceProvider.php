<?php

namespace Modules\Product\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Product\Entities\Product;
use Modules\Product\Observers\ProductObserver;

class ProductServiceProvider extends ModuleServiceProvider
{
    protected $module = 'product';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Product::observe(ProductObserver::class);
    }
}
