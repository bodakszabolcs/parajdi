<?php

namespace Modules\ProductVariation\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Product\Entities\Product;
		use Modules\Color\Entities\Color;
		use Modules\Type\Entities\Type;
		use Modules\Pack\Entities\Pack;


class ProductVariationViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "product_id" => $this->product_id,
		    "color_id" => $this->color_id,
		    "pack_id" => $this->pack_id,
		    "type_id" => $this->type_id,
		    "sku" => $this->sku,
		    "main_image" => $this->main_image,
		    "weight" => $this->weight,
		    "info" => $this->info,
		    "" => $this->in_stock,
		    "price" => $this->price,
		    "discount_price" => $this->discount_price,
		    "discount_from" => $this->discount_from,
		    "discount_to" => $this->discount_to,
            "tags" => $this->tags,
            "active"=> $this->active,
            "images"=> ($this->images)?$this->images:[]

		     ];
    }
}
