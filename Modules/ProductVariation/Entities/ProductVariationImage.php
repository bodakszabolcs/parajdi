<?php

namespace Modules\ProductVariation\Entities;

use Illuminate\Database\Eloquent\Model;



class ProductVariationImage extends Model
{
    protected $table = 'product_variation_image';

    protected $fillable = [
        'url'
    ];
    protected $dates = ['created_at', 'updated_at'];

}
