<?php

namespace Modules\ProductVariation\Entities;

use Modules\ProductVariation\Entities\Base\BaseProductVariation;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class ProductVariation extends BaseProductVariation
{
    use SoftDeletes, Cachable;




    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }
    public function getPrice(){
        if($this->discount_from <= date('Y-m-d H:i:s') && $this->discount_until >= date('Y-m-d H:i:s')){
            return $this->discount_price;
        }
        return $this->price;
    }








}
