<?php

    namespace Modules\ProductVariation\Entities\Base;
    use App\BaseModel;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use GeneaLabs\LaravelModelCaching\Traits\Cachable;
    use Spatie\Translatable\HasTranslations;
    use Modules\Product\Entities\Product;
    use Modules\Color\Entities\Color;
    use Modules\Type\Entities\Type;
    use Modules\Pack\Entities\Pack;
    abstract class BaseProductVariation extends BaseModel
    {
        protected $table = 'product_variations';
        protected $dates = ['created_at', 'updated_at'];

        public function __construct(array $attributes = [])
        {
            parent::__construct($attributes);
        }

        public function getFilters()
        {
            $this->searchColumns = [
                [
                    'name' => 'color_id', 'title' => 'Color', 'type' => 'select', 'data' => Color::pluck('name', 'id')
                ], [
                    'name' => 'sku', 'title' => 'SKU', 'type' => 'text',
                ], [
                    'name' => 'weight', 'title' => 'Weight', 'type' => 'text',
                ],
            ];

            return parent::getFilters();
        }

        protected $with = [ ];
        protected $fillable = [
            'product_id', 'color_id', 'pack_id', 'type_id', 'sku', 'main_image', 'weight', 'info', 'in_stock', 'price',
            'discount_price', 'discount_from', 'discount_to','tags','active'
        ];
        protected $casts = ['tags'=>'array'];

        public function product()
        {
            return $this->hasOne('Modules\Product\Entities\Product', 'id', 'product_id');
        }

        public function images()
        {
            return $this->hasMany('Modules\ProductVariation\Entities\ProductVariationImage', 'variation_id', 'id');
        }
        public function color()
        {
            return $this->hasOne('Modules\Color\Entities\Color', 'color_id', 'id');
        }

        public function type()
        {
            return $this->hasOne('Modules\Type\Entities\Type', 'type_id', 'id');
        }

        public function pack()
        {
            return $this->hasOne('Modules\Pack\Entities\Pack', 'pack_id', 'id');
        }
    }
