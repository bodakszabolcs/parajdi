<?php

namespace Modules\ProductVariation\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductVariationCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
			'pack_id' => 'required',
			'sku' => 'required',
			'main_image' => 'required',
			'weight' => 'required',
			'in_stock' => 'required',
			'price' => 'required',
			'discount_price' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'product_id' => __('Product'),
'color_id' => __('Color'),
'pack_id' => __('Pack'),
'type_id' => __('Type'),
'sku' => __('SKU'),
'main_image' => __('main_image'),
'weight' => __('Weight'),
'info' => __('Info'),
'in_stock' => __('In stock'),
'price' => __('Price'),
'discount_price' => __('Discount price'),
'discount_from' => __('Discount start'),
'discount_to' => __('Discount until'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
