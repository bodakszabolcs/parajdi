<?php

namespace Modules\ProductVariation\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProductVariation\Entities\ProductVariation;
use Illuminate\Http\Request;
use Modules\ProductVariation\Http\Requests\ProductVariationCreateRequest;
use Modules\ProductVariation\Http\Requests\ProductVariationUpdateRequest;
use Modules\ProductVariation\Transformers\ProductVariationViewResource;
use Modules\ProductVariation\Transformers\ProductVariationListResource;

class ProductVariationController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProductVariation();
        $this->viewResource = ProductVariationViewResource::class;
        $this->listResource = ProductVariationListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProductVariationCreateRequest::class;
        $this->updateRequest = ProductVariationUpdateRequest::class;
    }

}
