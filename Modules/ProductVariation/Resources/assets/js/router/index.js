import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProductVariation from '../components/ProductVariation'
import ProductVariationList from '../components/ProductVariationList'
import ProductVariationCreate from '../components/ProductVariationCreate'
import ProductVariationEdit from '../components/ProductVariationEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'product-variation',
                component: ProductVariation,
                meta: {
                    title: 'ProductVariations'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProductVariationList',
                        component: ProductVariationList,
                        meta: {
                            title: 'ProductVariations',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/product-variation/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-variation/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProductVariationCreate',
                        component: ProductVariationCreate,
                        meta: {
                            title: 'Create ProductVariations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-variation/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProductVariationEdit',
                        component: ProductVariationEdit,
                        meta: {
                            title: 'Edit ProductVariations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-variation/index'
                        }
                    }
                ]
            }
        ]
    }
]
