<?php
    /* @var $factory \Illuminate\Database\Eloquent\Factory */
    use Faker\Generator as Faker;
    use Modules\ProductVariation\Entities\ProductVariation;
    $factory->define(ProductVariation::class, function (Faker $faker) {
        $tags = [
            'New',
            'Best Seller',
            'Sale',
        ];
        return [
            "product_id" =>  \Modules\Product\Entities\Product::inRandomOrder()->first()->id,
            "color_id" => \Modules\Color\Entities\Color::inRandomOrder()->first()->id,
            "pack_id" => \Modules\Pack\Entities\Pack::inRandomOrder()->first()->id,
            "type_id" => \Modules\Type\Entities\Type::inRandomOrder()->first()->id,
            "sku" => $faker->ean13(),
            "main_image" => 'https://source.unsplash.com/800x800/?salt&'.rand(0,9999),
            "weight" => rand(0, 100)/10,
            "info" => $faker->realText(),
            "in_stock" => rand(0, 10),
            "price" => rand(1000, 15000),
            "discount_price" => rand(1000, 14000),
            "discount_from" => date('Y-m-d H:i:s',strtotime('-'.rand(0,500).' days' )),
            "discount_to" => date('Y-m-d H:i:s',strtotime((rand(0,1)?'-':'+').rand(0,20).' days' )),
            "tags" => [$tags[rand(0,2)]],
            "active" => 1
        ];
    });
