<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    class CreateProductVariationTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::dropIfExists('product_variation');
            Schema::dropIfExists('product_variation_images');
            Schema::create('product_variations', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->bigInteger("product_id")->unsigned()->nullable();
                $table->integer("color_id")->unsigned()->nullable();
                $table->integer("pack_id")->unsigned()->nullable();
                $table->integer("type_id")->unsigned()->nullable();
                $table->text("sku")->nullable();
                $table->text("main_image")->nullable();
                $table->float("weight")->nullable();
                $table->text("info")->nullable();
                $table->text("tags")->nullable();
                $table->integer("in_stock")->unsigned()->nullable();
                $table->float("price")->nullable();
                $table->float("discount_price")->nullable();
                $table->timestamp("discount_from")->nullable();
                $table->timestamp("discount_to")->nullable();
                $table->tinyInteger("active")->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(["deleted_at"]);

            });
            Schema::create('product_variation_image', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->bigInteger("variation_id")->unsigned()->nullable();
                $table->text("url")->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('product_variations');
            Schema::dropIfExists('product_variation_image');
        }
    }
