<?php

namespace Modules\ProductVariation\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProductVariation\Entities\ProductVariation;
use Modules\ProductVariation\Observers\ProductVariationObserver;

class ProductVariationServiceProvider extends ModuleServiceProvider
{
    protected $module = 'productvariation';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProductVariation::observe(ProductVariationObserver::class);
    }
}
