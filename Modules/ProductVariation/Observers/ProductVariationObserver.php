<?php

namespace Modules\ProductVariation\Observers;

use Modules\ProductVariation\Entities\ProductVariation;

class ProductVariationObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function saved(ProductVariation $model)
    {
        ProductVariation::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function created(ProductVariation $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function updated(ProductVariation $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function deleted(ProductVariation $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function restored(ProductVariation $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProductVariation\Entities\ProductVariation  $model
     * @return void
     */
    public function forceDeleted(ProductVariation $model)
    {
        //
    }
}
