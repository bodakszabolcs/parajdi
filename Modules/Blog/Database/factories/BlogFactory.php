<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Blog\Entities\Blog;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->realText(10),
        'meta_title' => $faker->unique()->realText(10),
        'slug' => $faker->unique()->slug,
        'lead' => $faker->realText(50),
        'meta_description' => $faker->realText(50),
        'content' => $faker->realText(),
        'user_id' => \Modules\User\Entities\User::first()->id,
        'main_image' => 'https://source.unsplash.com/1600x900/?nature,city'.rand(0,9999),
        'publication_date' => $faker->dateTimeBetween('-120 days', '+15 days')
    ];
});
