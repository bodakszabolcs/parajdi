<?php

namespace Modules\Blog\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Http\Requests\BlogCreateRequest;
use Modules\Blog\Transformers\BlogListResource;
use Modules\Blog\Transformers\BlogViewResource;
use Modules\Category\Entities\Category;
use Modules\Category\Entities\Tag;
use Modules\Category\Transformers\CategoryListResource;
use Modules\Category\Transformers\CategoryTreeResource;

class BlogController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Blog();
        $this->listResource = BlogListResource::class;
        $this->viewResource = BlogViewResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = BlogCreateRequest::class;
        $this->updateRequest = BlogCreateRequest::class;
    }

    public function credentials(Request $request)
    {
        $tags = Tag::pluck('name', 'id');

        return response()->json(['tags' => $tags, 'categories' => CategoryTreeResource::collection(Category::where('parent_id','=',null)->get())], $this->successStatus);
    }

    public function getList(Request $request, $categorySlug = null)
    {
        $blog = Blog::with('categories')->where('publication_date', '<=', date("Y-m-d H:i:s"));

        if (!is_null($categorySlug)) {
            $blog = $blog->whereHas('categories', function ($query) use ($categorySlug) {
                $query->where('slug', '=', $categorySlug);
            });
        }

        if ($request->has('category')) {
            $blog = $blog->whereHas('categories', function ($query) use ($request) {
                $query->where('slug', '=', $request->input('category'));
            });
        }
        if ($request->has('search')) {
            $blog = $blog->where( function ($query) use ($request) {
                $query->where(DB::raw('LOWER(`title`)'), 'LIKE', '%'.strtolower($request->input('search')).'%')
                    ->orWhere(DB::raw('LOWER(`lead`)'),'LIKE','%'.strtolower($request->input('search')).'%');

            });
        }
        if ($request->has('tag')) {
            $blog = $blog->where('tags', 'LIKE', '%' . $request->input('tag') . '%');
        }

        if ($request->has('archive')) {
            $blog = $blog->where('publication_date','LIKE','%'.$request->input('archive').'%');
        }

        $blog = $blog->orderBy('publication_date', 'DESC');

        $categories = Category::all();

        $archives = DB::table('blog')->select('publication_date')
            ->where('publication_date','<=', date("Y-m-01"))
            ->groupBy(DB::raw('YEAR(publication_date)'))
            ->groupBy(DB::raw('MONTH(publication_date)'))
            ->limit(6)->get()->pluck('publication_date');


        $latest = Blog::where('publication_date', '<=', date("Y-m-d H:i:s"))->orderBy('publication_date','DESC')->limit(3)->get();

        $archArr = [];
        foreach ($archives as $ar) {
            $archArr[date("Y-m",strtotime($ar))] = date("Y",strtotime($ar)).' '.__(date("F", strtotime($ar)));
        }

        return BlogListResource::collection($blog->paginate(6))->additional([
            'categories' => CategoryListResource::collection($categories),
            'archives' => $archArr,
            'latest' => BlogListResource::collection($latest)]);
    }

    public function getBlog(Request $request, $slug)
    {
        try {
            $blog = Blog::where('slug','=', $slug)->firstOrFail();
            $categories = Category::all();
            $archives = DB::table('blog')->select('publication_date')
                ->where('publication_date','<=', date("Y-m-01"))
                ->groupBy(DB::raw('YEAR(publication_date)'))
                ->groupBy(DB::raw('MONTH(publication_date)'))
                ->limit(6)->get()->pluck('publication_date');
            $archArr = [];
            foreach ($archives as $ar) {
                $archArr[date("Y-m",strtotime($ar))] = date("Y",strtotime($ar)).' '.__(date("F", strtotime($ar)));
            }
            $recent = Blog::where('publication_date', '<=', date("Y-m-d H:i:s"))->inRandomOrder()->limit(3)->get();
            return BlogListResource::collection([$blog])->additional([
                'categories' => CategoryListResource::collection($categories),
                'archives' => $archArr,
                'prev' => new BlogListResource(Blog::where('id','<>',$blog->id)->inRandomOrder()->first()),
                'next' => new BlogListResource(Blog::where('id','<>',$blog->id)->inRandomOrder()->first()),
                'recent' => BlogListResource::collection($recent)]);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
    }
}
