import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Blog from '../components/Blog'
import BlogList from '../components/BlogList'
import BlogCreate from '../components/BlogCreate'
import BlogEdit from '../components/BlogEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'blog',
                component: Blog,
                meta: {
                    title: 'Blog'
                },
                children: [
                    {
                        path: 'index',
                        name: 'BlogList',
                        component: BlogList,
                        meta: {
                            title: 'Blog',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/blog/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/blog/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'BlogCreate',
                        component: BlogCreate,
                        meta: {
                            title: 'Create Blog',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/blog/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'BlogEdit',
                        component: BlogEdit,
                        meta: {
                            title: 'Edit Blog',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/blog/index'
                        }
                    }
                ]
            }
        ]
    }
]
