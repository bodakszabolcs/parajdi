<?php

return [
    'name' => 'Blog',
    'menu_order' => 18,
    'menu' => [
        [
            'icon' => 'la la-clipboard',
            'title' => 'Content',
            'route' => '#content',
            'submenu' => [
                [
                    'icon' => 'la la-book',
                    'title' => 'Blog',
                    'route' => '/'.env('ADMIN_URL').'/blog/index',
                ],
                [
                    'icon' => 'la la-list-ul',
                    'title' => 'Categories',
                    'route' => '/'.env('ADMIN_URL').'/category/index'
                ],
                [
                    'icon' => 'la la-tags',
                    'title' => 'Tags',
                    'route' => '/'.env('ADMIN_URL').'/tag/index'
                ]
            ]
        ]
    ]
];
