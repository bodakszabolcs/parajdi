<?php

namespace Modules\Blog\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\Category\Entities\Tag;
use Modules\System\Entities\Settings;
use Modules\User\Entities\User;

abstract class BaseBlog extends BaseModel
{
    protected $table = 'blog';

    protected $fillable = [
        'title',
        'slug',
        'lead',
        'content',
        'main_image',
        'meta_title',
        'meta_description',
        'publication_date'
    ];

    protected $with = [
        'categories',
        'tags',
        'user'
    ];

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'title',
                'title' => __('Title'),
                'type' => 'text',
            ],
            [
                'name' => 'publication_date',
                'title' => __('Publication date'),
                'type' => 'date_interval',
            ],
            [
                'name' => 'user_id',
                'title' => __('Author'),
                'type' => 'select',
                'data' => User::select(DB::raw('CONCAT(lastname," ",firstname) as full_name'), 'id')->pluck('full_name', 'id')
            ]
        ];

        return parent::getFilters();
    }

    public function fillAndSave(array $request)
    {
        $lang = Settings::where('settings_key', '=', 'language')->first();
        $languages = json_decode($lang->settings_value, true);

        $slugArr = [];
        foreach ($languages as $key => $val) {
            if (empty(Arr::get($request, 'slug.' . $key, null))) {
                $slugArr[$key] = $this->slugify($this->getTranslation('title',$key).' '.Arr::get($request, 'publication_date'));
            } else {
                $slugArr[$key] = $this->slugify(Arr::get($request, 'slug.' . $key, null));
            }
        }

        $categories = [];
        foreach (Arr::get($request, 'categories', []) as $key => $value) {
            if (!is_null($value) && $value === true) {
                $categories[] = $key;
            }
        }

        $tagArr = [];
        foreach (Arr::get($request, 'tags', []) as $tagKey => $tagValue) {
            if (Arr::get($tagValue, 'text', null) !== null) {
                $tagKey = Arr::get($tagValue, 'id', null);
                $tagValue = Arr::get($tagValue, 'text', null);
            }
            $tg = Tag::where('id','=',$tagKey)->first();

            if (is_null($tg)) {
                $tg = new Tag();
                $tg->name = $tagValue;
                $tg->slug = Str::slug($tagValue);
                $tg->save();
            }

            $tagArr[] = $tg->id;
        }

        $this->fill($request);
        $this->setTranslations('slug',$slugArr);
        $this->save();
        $this->categories()->sync($categories);
        $this->tags()->sync($tagArr);

        return $this;
    }

    public function categories()
    {
        return $this->belongsToMany('Modules\Category\Entities\Category', 'blog_categories', 'blog_id',
            'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany('Modules\Category\Entities\Tag', 'blog_tags', 'blog_id',
            'tags_id');
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id', 'id');
    }
}
