<?php

namespace Modules\Blog\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Blog\Entities\Base\BaseBlog;
use Spatie\Translatable\HasTranslations;

class Blog extends BaseBlog
{
    use SoftDeletes, Cachable, HasTranslations;

    public $translatable = ['title', 'meta_title', 'meta_description', 'content', 'lead'];
}
