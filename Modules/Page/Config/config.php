<?php

return [
    'name' => 'Page',
    'menu_order' => 16,
    'menu' => [
        [
            'icon' => 'la la-clipboard',
            'title' => 'Content',
            'route' => '#content',
            'submenu' => [
                [
                    'icon' => 'la la-windows',
                    'title' => 'Pages',
                    'route' => '/'.env('ADMIN_URL').'/page/index'
                ]
            ]
        ]
    ]
];
