<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Page\Entities\Page;

class PageDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $pg = new Page();
        $pg->name = 'HomePage';
        $pg->meta_title = 'HomePage';
        $pg->slug = '';
        $pg->content = 'Home Page content';
        $pg->meta_description = 'Home Page content';
        $pg->component_name = 'HomePage.vue';
        $pg->save();

        $pg = new Page();
        $pg->name = 'Contact';
        $pg->meta_title = 'Contact';
        $pg->slug = 'contact';
        $pg->content = 'Contact Page content';
        $pg->meta_description = 'Contact Page content';
        $pg->save();

        $pg = new Page();
        $pg->name = 'Privacy';
        $pg->meta_title = 'Privacy';
        $pg->slug = 'privacy';
        $pg->content = 'Privacy Page content';
        $pg->meta_description = 'Privacy Page content';
        $pg->save();

        $pg = new Page();
        $pg->name = 'User Agreement';
        $pg->meta_title = 'User Agreement';
        $pg->slug = 'user-agreement';
        $pg->content = 'User Agreement Page content';
        $pg->meta_description = 'User Agreement Page content';
        $pg->save();

    }
}
