<?php

namespace Modules\Page\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Page\Entities\Page;
use Modules\System\Entities\Settings;
use Modules\Webshop\Entities\Product;

class GenerateSiteMap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'generate:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sitemap for every url.';

    private $xml;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $slugs = [];

        $lang = Settings::where('settings_key','=','language')->first();
        $languages = json_decode($lang->settings_value, true);
        $prefixes = json_decode(Storage::disk(config('filesystems.default_upload_filesystem'))->get('prefixes.json'), true);

        foreach ($languages as $key => $val) {
            App::setLocale($key);

            foreach (Page::all() as $model) {
                if (in_array(url($model->slug), $slugs)) {
                    continue;
                } else {
                    $slugs[] = url($model->slug);
                }
                $this->xml .= '<url>
  <loc>'.url($model->slug).'</loc>
  <lastmod>'.date("Y-m-d", strtotime($model->updated_at)).'T'.date("H:i:s", strtotime($model->updated_at)).'+00:00</lastmod>
</url>';
            }

            foreach (Category::all() as $model) {
                if (in_array(url(Arr::get($prefixes, 'category',null).'/'.$model->slug), $slugs)) {
                    continue;
                } else {
                    $slugs[] = url(Arr::get($prefixes, 'category',null).'/'.$model->slug);
                }
                $this->xml .= '<url>
  <loc>'.url(Arr::get($prefixes, 'category',null).'/'.$model->slug).'</loc>
  <lastmod>'.date("Y-m-d", strtotime($model->publication_date)).'T'.date("H:i:s", strtotime($model->publication_date)).'+00:00</lastmod>
</url>';
            }

            foreach (Blog::where('publication_date','<=',now())->get() as $model) {
                if (in_array(url(Arr::get($prefixes, 'blog',null).'/'.$model->slug), $slugs)) {
                    continue;
                } else {
                    $slugs[] = url(Arr::get($prefixes, 'blog',null).'/'.$model->slug);
                }
                $this->xml .= '<url>
  <loc>'.url(Arr::get($prefixes, 'blog',null).'/'.$model->slug).'</loc>
  <lastmod>'.date("Y-m-d", strtotime($model->publication_date)).'T'.date("H:i:s", strtotime($model->publication_date)).'+00:00</lastmod>
</url>';
            }

            foreach (Product::where('enabled','=',1)->get() as $product) {
                $this->xml .= '<url>
  <loc>'.url(Arr::get($prefixes, 'product',null).'/'.$product->slug).'</loc>
  <lastmod>'.date("Y-m-d", strtotime($product->updated_at)).'T'.date("H:i:s", strtotime($product->updated_at)).'+00:00</lastmod>
</url>';
            }
        }

        $this->xml .= '<url><loc>'.url(Arr::get($prefixes, 'shop', null)).'</loc><lastmod>'.date("Y-m-d").'T'.date("H:i:s").'</lastmod></url>';

        $this->xml .= '</urlset>';

        Storage::disk('public_folder')->put('sitemap.xml', $this->xml);
        $this->info('Sitemap generated!');
    }
}
