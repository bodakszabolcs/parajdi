<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Page\Http\Controllers',
    'prefix' => '/page'
], function () {
    Route::get( '/get-components', 'PageController@getComponents')->name('Get components');
    Route::get( '/{slug?}', 'PageController@getBySlug')->name('Get Page by slug');
});

Route::group([
    'namespace' => 'Modules\Page\Http\Controllers',
    'prefix' => '/page',
    'middleware' => ['auth:sanctum', 'accesslog', 'role']
], function () {
    Route::put( '/save-page-data/{id}', 'PageController@savePageData')->name('Save page data');
});
