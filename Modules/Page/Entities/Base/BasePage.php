<?php

namespace Modules\Page\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Blog\Entities\Blog;
use Modules\Category\Entities\Category;
use Modules\Page\Entities\Page;
use Modules\System\Entities\Settings;
use PHPHtmlParser\Dom;

abstract class BasePage extends BaseModel
{
    protected $table = 'pages';

    protected $fillable = [
        'name',
        'slug',
        'content',
        'meta_title',
        'meta_description',
        'og_image',
        'component_name'
    ];

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        if (Arr::get($request, 'slug', null) == null) {
            $this->slug = $this->slugify($this->name.' '.$this->id);
        }

        $this->save();

        return $this;
    }

    public function sanitizeContent($key, $content) {
        $dom = new Dom();
        $dom->load($content);

        return $dom->find('*[data-name='.$key.']')[0]->innerHtml;
    }

    public function getTemplateSchemes() {
        if (is_null($this->component_name) || (is_string($this->content) && json_decode($this->content) === false)) {
            return false;
        }

        $baseScheme = [];

        $content = Storage::disk('resources')->get('js/frontend/components/pages/' . $this->component_name);

        $vueContent = $this->get_string_between($content, '<template>','</template>');

        $vueContent = trim(str_replace("\n",'',$vueContent));

        $dom = new Dom();
        $dom->load($vueContent);

        $elements = $dom->find('*[data-name]');

        foreach ( $elements as $elem) {
            $keyName = $elem->getAttribute('data-name');
            $html = $elem->innerHtml;

            $baseScheme[$keyName] = $html;
        }

        $this->content = json_encode($baseScheme);
        $this->saveWithoutEvents();
    }

    public function saveEditorData($request) {
        $content = $this->content;

        if (json_decode($content) !== false) {
            $content = json_decode($content, true);
        } else {
            $content = [];
        }

        foreach ($request->all() as $key => $val) {
            $content[$key] = $this->sanitizeContent($key, $val);
        }

        $this->content = json_encode($content);
        $this->save();
    }

    public static function returnFrontendContent($request)
    {
        $meta = '<title>' . config('app.name', 'Laravel') . '</title>';
        $meta .= '<meta name="robots" content="noindex" />';
        $path = $request->path();
        $replacedPath = Str::replaceFirst("/", "", $path);

        /* Return Static Page */

        $page = Page::where(function ($query) use ($path, $replacedPath) {
            $query->where('slug', '=', $path)
                ->orWhere('slug', '=', $replacedPath);
        })->first();

        if (!is_null($page)) {


            self::setMetaTags($meta, $request->getUri(), $page);

            return view('layouts.app', ['meta' => $meta]);

        }
        /* End of Return Static Page */

        $prefixes = json_decode(Storage::disk(config('filesystems.default_upload_filesystem'))->get('prefixes.json'), true);

        /* Return Blog Page */
        $blog = Blog::where('slug', 'LIKE', Str::replaceFirst(Arr::get($prefixes, 'blog',null) . "/", "", $path))->first();

        if (!is_null($blog)) {
            self::setMetaTags($meta, $request->getUri(), $blog);
            return view('layouts.app', ['meta' => $meta]);
        }
        /* End of Return Blog Page */

        /* Return Category Page */
        $category = Category::where('slug', 'LIKE',
            Str::replaceFirst(Arr::get($prefixes, 'category',null) . "/", "", $path))->first();

        if (!is_null($category)) {
            self::setMetaTags($meta, $request->getUri(), $category);
            return view('layouts.app', ['meta' => $meta]);
        }
        /* End of Return Category Page */

        return response()->view('layouts.app', ['meta' => $meta])->setStatusCode(404);
    }

    /**
     * Generate SEO tags
     * @param $meta
     * @param $url
     * @param $data
     */
    private static function setMetaTags(&$meta, $url, $data)
    {
        $appName = $data->meta_title . ' - ' . config('app.name', 'Laravel');
        $meta = '<title>' . $appName . '</title>';
        $meta .= '<meta name="twitter:card" content="summary" />';
        $meta .= '<meta name="twitter:site" content="@creativeagent" />';
        $meta .= '<meta name="description" content="' . $data->meta_description . '">';
        $meta .= '<meta name="og:title" content="' . $appName . '">';
        $meta .= '<meta name="og:url" content="' . $url . '">';
        if (isset($data->og_image)) {
            $meta .= '<meta name="og:image" content="' . ((!Str::startsWith($data->og_image,
                    'http')) ? url($data->og_image) : $data->og_image) . '">';
        } else {
            $meta .= '<meta name="og:image" content="' . url('/frontend/img/logo.svg') . '">';
        }
        if (isset($data->tags) && is_array($data->tags)) {
            $meta .= '<meta name="keywords" content="' . implode(",", $data->tags) . '">';
        }
    }

    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}
