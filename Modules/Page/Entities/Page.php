<?php

namespace Modules\Page\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Page\Entities\Base\BasePage;
use Spatie\Translatable\HasTranslations;

class Page extends BasePage
{
    use SoftDeletes, Cachable, HasTranslations;

    public $translatable = ['name', 'meta_title', 'meta_description', 'content', 'component_name'];
}
