<?php

namespace Modules\Page\Observers;

use Modules\Page\Entities\Page;

class PageObserver
{
    /**
     * Handle the blog "created" event.
     *
     * @param  \Modules\Page\Entities\Page  $page
     * @return void
     */
    public function created(Page $page)
    {
        $page->getTemplateSchemes();
    }

    /**
     * Handle the blog "updated" event.
     *
     * @param  \Modules\Page\Entities\Page  $page
     * @return void
     */
    public function updated(Page $page)
    {
        $page->getTemplateSchemes();
    }
}
