<?php

namespace Modules\Page\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Modules\Page\Entities\Page;
use Modules\Page\Http\Requests\PageCreateRequest;
use Modules\Page\Transformers\PageListResource;
use Modules\Page\Transformers\PageViewResource;

class PageController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Page();
        $this->listResource = PageListResource::class;
        $this->viewResource = PageViewResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PageCreateRequest::class;
        $this->updateRequest = PageCreateRequest::class;
    }

    public function getBySlug(Request $request, $slug = '')
    {
        try {
            return new $this->listResource(Page::where('slug','=', $slug)->firstOrFail());
        } catch (ModelNotFoundException $e)
        {
            abort(404);
        }
    }

    public function getComponents(Request $request)
    {
        $filesArr = [];
        $files = Storage::disk('resources')->files('js/frontend/components/pages');

        foreach ($files as $f) {
            $file = explode("/",$f);
            if (end($file) === 'readme.md') continue;
            $filesArr[end($file)] = end($file);
        }


        return response()->json(['files' => $filesArr], $this->successStatus);
    }

    public function savePageData(Request $request, $id) {
        try {
                $page = Page::findOrfail($id);
                $page->saveEditorData($request);

            return response()->json(['status' => 'OK'], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort($this->errorStatus);
        }
    }

}
