import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Console from './../components/Console'
import Settings from './../components/Settings'
import Media from './../components/Media'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'system',
                component: Settings,
                meta: {
                    title: 'Settings',
                    subheader: true
                }
            },
            {
                path: 'console',
                component: Console,
                meta: {
                    title: 'Console',
                    subheader: true
                }
            },
            {
                path: 'media',
                component: Media,
                meta: {
                    title: 'Media',
                    subheader: true
                }
            }
        ]
    }
]
