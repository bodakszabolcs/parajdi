<?php

namespace Modules\System\Entities\Base;

use App\BaseModel;

class BaseSettings extends BaseModel
{
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'settings_key',
        'settings_value'

    ];
}
