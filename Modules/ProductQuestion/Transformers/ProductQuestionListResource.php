<?php

namespace Modules\ProductQuestion\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProductQuestionListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "product" => $this->product->name,
		    "answer" => $this->answer,
		    "moderated" => ($this->moderated)?__('Yes'):__('No'),
		    "useful" => $this->useful,
		    "not_useful" => $this->not_useful,
		    "email" => $this->email,
		    "name" => $this->name,
		    "title" => $this->title,
		    "comment" => $this->comment,
		     ];
    }
}
