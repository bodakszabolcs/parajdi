<?php

namespace Modules\ProductQuestion\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Product\Entities\Product;
use Modules\Product\Transformers\ProductListResource;
class ProductQuestionViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "product" => new ProductListResource($this->product),
            "product_id" => $this->product_id,
		    "answer" => $this->answer,
		    "moderated" => $this->moderated,
		    "useful" => $this->useful,
		    "not_useful" => $this->not_useful,
		    "email" => $this->email,
		    "name" => $this->name,
		    "title" => $this->title,
		    "comment" => $this->comment,
            "selectables" => [
                 "product" => Product::pluck("name","id"),
            ]
		     ];
    }
}
