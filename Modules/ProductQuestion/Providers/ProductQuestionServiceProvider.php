<?php

namespace Modules\ProductQuestion\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProductQuestion\Entities\ProductQuestion;
use Modules\ProductQuestion\Observers\ProductQuestionObserver;

class ProductQuestionServiceProvider extends ModuleServiceProvider
{
    protected $module = 'productquestion';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProductQuestion::observe(ProductQuestionObserver::class);
    }
}
