import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProductQuestion from '../components/ProductQuestion'
import ProductQuestionList from '../components/ProductQuestionList'
import ProductQuestionCreate from '../components/ProductQuestionCreate'
import ProductQuestionEdit from '../components/ProductQuestionEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'productquestion',
                component: ProductQuestion,
                meta: {
                    title: 'ProductQuestions'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProductQuestionList',
                        component: ProductQuestionList,
                        meta: {
                            title: 'ProductQuestions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productquestion/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProductQuestionCreate',
                        component: ProductQuestionCreate,
                        meta: {
                            title: 'Create ProductQuestions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productquestion/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProductQuestionEdit',
                        component: ProductQuestionEdit,
                        meta: {
                            title: 'Edit ProductQuestions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productquestion/index'
                        }
                    }
                ]
            }
        ]
    }
]
