<?php

namespace Modules\ProductQuestion\Observers;

use Modules\ProductQuestion\Entities\ProductQuestion;

class ProductQuestionObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function saved(ProductQuestion $model)
    {
        ProductQuestion::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function created(ProductQuestion $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function updated(ProductQuestion $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function deleted(ProductQuestion $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function restored(ProductQuestion $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProductQuestion\Entities\ProductQuestion  $model
     * @return void
     */
    public function forceDeleted(ProductQuestion $model)
    {
        //
    }
}
