<?php

namespace Modules\ProductQuestion\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Webshop\Entities\Product;


abstract class BaseProductQuestion extends BaseModel
{


    protected $table = 'product_questions';

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
                   [
                    'name' => 'email',
                    'title' => 'Email',
                    'type' => 'text',
                    ],
                   [
                       'name' => 'moderated',
                       'title' => 'Moderated',
                       'type' => 'select',
                       'data' => ['0'=>__('No'),'1' =>'Yes']
                   ],
                   [
                    'name' => 'name',
                    'title' => 'Name',
                    'type' => 'text',
                    ],[
                    'name' => 'title',
                    'title' => 'Title',
                    'type' => 'text',
                    ],[
                    'name' => 'comment',
                    'title' => 'Comment',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $fillable = ['product_id','answer','moderated','useful','not_useful','email','name','title','comment'];

    protected $casts = [];

     public function product()
    {
        return $this->hasOne('Modules\Product\Entities\Product','id','product_id');
    }



}
