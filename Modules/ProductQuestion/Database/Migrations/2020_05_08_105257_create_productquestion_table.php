<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('product_questions');
        Schema::create('product_questions', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("product_id")->unsigned()->nullable();
            $table->text("answer")->nullable();
            $table->tinyInteger("moderated")->unsigned()->nullable();
            $table->integer("useful")->unsigned()->nullable();
            $table->integer("not_useful")->unsigned()->nullable();
            $table->text("email")->nullable();
            $table->text("name")->nullable();
            $table->text("title")->nullable();
            $table->text("comment")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(["product_id", "deleted_at"]);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_questions');
    }
}
