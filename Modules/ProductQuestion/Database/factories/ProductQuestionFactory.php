<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\ProductQuestion\Entities\ProductQuestion;

$factory->define(ProductQuestion::class, function (Faker $faker) {
    return [
        "product_id" => rand(1,100),
        "answer" => $faker->realText(),
        "moderated" => 1,
        "useful" => rand(1,50),
        "not_useful" => rand(1,50),
        "email" => $faker->email(),
        "name" => $faker->name(),
        "title" => $faker->word(),
        "comment" => $faker->realText(),

    ];
});
