<?php

namespace Modules\ProductQuestion\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProductQuestion\Entities\ProductQuestion;
use Illuminate\Http\Request;
use Modules\ProductQuestion\Http\Requests\ProductQuestionCreateRequest;
use Modules\ProductQuestion\Http\Requests\ProductQuestionUpdateRequest;
use Modules\ProductQuestion\Transformers\ProductQuestionViewResource;
use Modules\ProductQuestion\Transformers\ProductQuestionListResource;

class ProductQuestionController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProductQuestion();
        $this->viewResource = ProductQuestionViewResource::class;
        $this->listResource = ProductQuestionListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProductQuestionCreateRequest::class;
        $this->updateRequest = ProductQuestionUpdateRequest::class;
    }

}
