<?php

return [
    'name' => 'ProductQuestion',

    'menu_order' => 21,

    'menu' => [
        [
            'icon' => 'la la-newspaper-o',
            'title' => 'Catalog',
            'route' => '#catalog',
            'submenu' => [
                [

                    'icon' => 'la la-question',

                    'title' => 'Product questions',

                    'route' => '/' . env('ADMIN_URL') . '/productquestion/index',

                ]
            ]
        ]

    ]
];
