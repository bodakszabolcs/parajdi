import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Color from '../components/Color'
import ColorList from '../components/ColorList'
import ColorCreate from '../components/ColorCreate'
import ColorEdit from '../components/ColorEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'color',
                component: Color,
                meta: {
                    title: 'Colors'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ColorList',
                        component: ColorList,
                        meta: {
                            title: 'Colors',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/color/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/color/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ColorCreate',
                        component: ColorCreate,
                        meta: {
                            title: 'Create Colors',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/color/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ColorEdit',
                        component: ColorEdit,
                        meta: {
                            title: 'Edit Colors',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/color/index'
                        }
                    }
                ]
            }
        ]
    }
]
