<?php

namespace Modules\Color\Entities;

use Modules\Color\Entities\Base\BaseColor;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Color extends BaseColor
{
    use SoftDeletes, Cachable, HasTranslations;

    public $translatable = ["name"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
