<?php
    return [
        'name' => 'Color', 'menu_order' => 13, 'menu' => [
            [
                'icon' => 'fa flaticon-map', 'title' => 'Product attributes', 'route' => '#prodAttributes', 'submenu' => [
                [
                    'icon' => 'la-palette', 'title' => 'Color', 'route' => '/' . env('ADMIN_URL') . '/color/index',
                ], [
                    'icon' => 'la la-gears', 'title' => 'Pack', 'route' => '/' . env('ADMIN_URL') . '/pack/index'
                ], [
                    'icon' => 'la la-terminal', 'title' => 'Type', 'route' => '/' . env('ADMIN_URL') . '/type/index'
                ], [
                    'icon' => 'la la-globe', 'title' => 'Product category', 'route' => '/' . env('ADMIN_URL') . '/product-category/index'
                ]
            ]
            ],
        ]
    ];
