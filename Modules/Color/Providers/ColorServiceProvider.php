<?php

namespace Modules\Color\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Color\Entities\Color;
use Modules\Color\Observers\ColorObserver;

class ColorServiceProvider extends ModuleServiceProvider
{
    protected $module = 'color';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Color::observe(ColorObserver::class);
    }
}
