<?php

namespace Modules\Color\Observers;

use Modules\Color\Entities\Color;

class ColorObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function saved(Color $model)
    {
        Color::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function created(Color $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function updated(Color $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function deleted(Color $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function restored(Color $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Color\Entities\Color  $model
     * @return void
     */
    public function forceDeleted(Color $model)
    {
        //
    }
}
