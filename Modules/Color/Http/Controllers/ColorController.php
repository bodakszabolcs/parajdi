<?php

namespace Modules\Color\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Color\Entities\Color;
use Illuminate\Http\Request;
use Modules\Color\Http\Requests\ColorCreateRequest;
use Modules\Color\Http\Requests\ColorUpdateRequest;
use Modules\Color\Transformers\ColorViewResource;
use Modules\Color\Transformers\ColorListResource;

class ColorController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Color();
        $this->viewResource = ColorViewResource::class;
        $this->listResource = ColorListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ColorCreateRequest::class;
        $this->updateRequest = ColorUpdateRequest::class;
    }

}
