<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Color\Entities\Color;

$factory->define(Color::class, function (Faker $faker) {
    return [
        "name" => $faker->unique()->text(8),
        "hexa" => generateHexa(),

    ];
});

