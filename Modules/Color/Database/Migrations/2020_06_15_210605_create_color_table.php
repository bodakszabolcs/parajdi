<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('colors');
        Schema::create('colors', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();
$table->text("hexa")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
    }
}
