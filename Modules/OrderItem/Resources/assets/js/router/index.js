import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import OrderItem from '../components/OrderItem'
import OrderItemList from '../components/OrderItemList'
import OrderItemCreate from '../components/OrderItemCreate'
import OrderItemEdit from '../components/OrderItemEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'order-items',
                component: OrderItem,
                meta: {
                    title: 'OrderItems'
                },
                children: [
                    {
                        path: 'index',
                        name: 'OrderItemList',
                        component: OrderItemList,
                        meta: {
                            title: 'OrderItems',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/order-items/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/order-items/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'OrderItemCreate',
                        component: OrderItemCreate,
                        meta: {
                            title: 'Create OrderItems',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/order-items/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'OrderItemEdit',
                        component: OrderItemEdit,
                        meta: {
                            title: 'Edit OrderItems',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/order-items/index'
                        }
                    }
                ]
            }
        ]
    }
]
