<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\OrderItem\Entities\OrderItem;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        "product_id" => rand(1000,5000),
"variation_id" => rand(1000,5000),
"unit_price" => rand(1000,5000),
"quantity" => $faker->realText(),
"order_id" => $faker->realText(),

    ];
});
