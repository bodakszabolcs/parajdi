<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('order_items');
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("product_id")->unsigned()->nullable();
$table->bigInteger("variation_id")->unsigned()->nullable();
$table->float("unit_price")->nullable();
$table->integer("quantity")->nullable();
$table->text("order_id")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
