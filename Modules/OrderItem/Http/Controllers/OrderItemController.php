<?php

namespace Modules\OrderItem\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\OrderItem\Entities\OrderItem;
use Illuminate\Http\Request;
use Modules\OrderItem\Http\Requests\OrderItemCreateRequest;
use Modules\OrderItem\Http\Requests\OrderItemUpdateRequest;
use Modules\OrderItem\Transformers\OrderItemViewResource;
use Modules\OrderItem\Transformers\OrderItemListResource;

class OrderItemController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new OrderItem();
        $this->viewResource = OrderItemViewResource::class;
        $this->listResource = OrderItemListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = OrderItemCreateRequest::class;
        $this->updateRequest = OrderItemUpdateRequest::class;
    }

}
