<?php

namespace Modules\OrderItem\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderItemCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
			'variation_id' => 'required',
			'unit_price' => 'required',
			'amount' => 'required',
			'order_id' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'product_id' => __('Product'),
'variation_id' => __('variation_id'),
'unit_price' => __('Unit price'),
'amount' => __('Amount'),
'order_id' => __('order_id'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
