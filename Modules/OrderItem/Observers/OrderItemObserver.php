<?php

namespace Modules\OrderItem\Observers;

use Modules\OrderItem\Entities\OrderItem;

class OrderItemObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function saved(OrderItem $model)
    {
        OrderItem::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function created(OrderItem $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function updated(OrderItem $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function deleted(OrderItem $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function restored(OrderItem $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\OrderItem\Entities\OrderItem  $model
     * @return void
     */
    public function forceDeleted(OrderItem $model)
    {
        //
    }
}
