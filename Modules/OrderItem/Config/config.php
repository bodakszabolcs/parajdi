<?php

return [
    'name' => 'OrderItem',

                 'menu_order' => 21,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'OrderItem',

                      'route' =>'/'.env('ADMIN_URL').'/order-items/index',

                     ]

                 ]
];
