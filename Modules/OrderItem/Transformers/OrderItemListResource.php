<?php

namespace Modules\OrderItem\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class OrderItemListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "product_id" => $this->product_id,
		    "variation_id" => $this->variation_id,
		    "unit_price" => $this->unit_price,
		    "amount" => $this->amount,
		    "order_id" => $this->order_id,
		     ];
    }
}
