<?php

namespace Modules\OrderItem\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\DB;
use Spatie\Translatable\HasTranslations;
use Modules\Product\Entities\Product;
		use Modules\ProductVariation\Entities\ProductVariation;


abstract class BaseOrderItem extends BaseModel
{


    protected $table = 'order_items';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'product_id',
                    'title' => 'Product',
                    'type' => 'text',
                    ],[
                    'name' => 'variation_id',
                    'title' => 'variation_id',
                    'type' => 'text',
                    ],[
                    'name' => 'unit_price',
                    'title' => 'Unit price',
                    'type' => 'text',
                    ],[
                    'name' => 'amount',
                    'title' => 'Amount',
                    'type' => 'text',
                    ],[
                    'name' => 'order_id',
                    'title' => 'order_id',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = ["product","variation"];

    protected $fillable = ['product_id','variation_id','unit_price','quantity','order_id'];

    protected $casts = [];

     public function product()
    {
        return $this->hasOne('Modules\Product\Entities\Product','id','product_id');
    }


 public function variation()
    {
        return $this->hasOne('Modules\ProductVariation\Entities\ProductVariation','id','variation_id')->leftJoin('colors','colors.id','color_id')
            ->leftJoin('types','types.id','type_id')
            ->leftJoin('packs','packs.id','pack_id')->select(DB::raw('product_variations.*, colors.name as color,types.name as type,packs.name as pack'));
    }



}
