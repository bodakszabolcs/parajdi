<?php

namespace Modules\OrderItem\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\OrderItem\Entities\OrderItem;
use Modules\OrderItem\Observers\OrderItemObserver;

class OrderItemServiceProvider extends ModuleServiceProvider
{
    protected $module = 'OrderItem';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        OrderItem::observe(OrderItemObserver::class);
    }
}
