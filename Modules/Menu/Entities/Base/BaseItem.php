<?php

namespace Modules\Menu\Entities\Base;

use App\BaseModel;

abstract class BaseItem extends BaseModel
{

    protected $table = 'menu_items';

    protected $fillable = [
        'order',
        'title',
        'link',
        'parent_id'
    ];

    public function menu()
    {
        return $this->belongsTo('Modules\Menu\Entities\Menu','menu_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('Modules\Menu\Entities\Item', 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany('Modules\Menu\Entities\Item', 'parent_id', 'id');
    }

}
