<?php

namespace Modules\Menu\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Menu\Entities\Item;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Transformers\MenuResource;
use Nwidart\Modules\Facades\Module;

abstract class BaseMenu extends BaseModel
{
    protected $table = 'menus';

    protected $fillable = [
        'name',
    ];

    protected $with = [
        'items'
    ];

    public function items()
    {
        return $this->hasMany('Modules\Menu\Entities\Item', 'menu_id', 'id');
    }

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        $this->items()->delete();
        $this->setChildrenMenuItems(null, $this->id, Arr::get($request, 'items', []));
        $this->update();

        return $this;
    }

    private function setChildrenMenuItems($parentId, $menuId, $items)
    {
        $order = 1;
        foreach ($items as $item) {
            $menuItem = new Item();
            $menuItem->menu_id = $menuId;
            $menuItem->title = $item['title'];
            $menuItem->order = $order;
            $menuItem->parent_id = $parentId;
            $menuItem->link = $item['link'];
            $menuItem->save();

            $this->setChildrenMenuItems($menuItem->id, $this->id, Arr::get($item, 'children', []));

            $order++;
        }
    }

    public function generateMenuJson()
    {
        $menu = Menu::find($this->id);
        Storage::disk(config('filesystems.default_upload_filesystem'))->put('menus/menu-' .config('app.env'). '-'. $this->id . '.json', json_encode(new MenuResource($menu), JSON_UNESCAPED_UNICODE), 'public');
    }

    public function deleteMenuJson()
    {
        Storage::disk(config('filesystems.default_upload_filesystem'))->delete('menus/menu-' .config('app.env'). '-'. $this->id . '.json');
    }

    public static function getMenu()
    {
        $finishedMenus = [];

        foreach (Module::allEnabled() as $module) {
            $config = config(Str::lower($module->getName()));
            if (isset($config['menu']) && isset($config['menu_order'])) {

                if (sizeof($finishedMenus) < 1) {
                    foreach ($config['menu'] as $m) {
                        $finishedMenus[$config['menu_order']] = $m;
                    }
                } else {
                    foreach ($config['menu'] as $m) {
                        $index = 0;
                        $have = false;
                        foreach ($finishedMenus as $key => $fm) {
                            if ($fm['title'] == $m['title'] && $fm['route'] == $m['route']) {
                                $finishedMenus[$key]['submenu'] = array_merge($finishedMenus[$key]['submenu'],
                                    $m['submenu']);
                                $have = true;
                            }
                            if ($have) {
                                break;
                            }
                            $index++;
                        }

                        if (!$have) {
                            $menuKey = self::newMenuId($finishedMenus, $config['menu_order']);
                            $finishedMenus[$menuKey] = $m;
                        }
                    }
                }
            }
        }

        ksort($finishedMenus);

        $sorted = [];
        foreach ($finishedMenus as $fm) {
            $sorted[] = $fm;
        }

        return $sorted;
    }

    private static function newMenuId($menu, $key) {
        if (array_key_exists($key, $menu)) {
            $key += 1;
            return self::newMenuId($menu, $key);
        }

        return $key;
    }

}
