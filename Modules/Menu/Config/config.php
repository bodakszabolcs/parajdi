<?php

return [
    'name' => 'Menu',
    'menu_order' => 19,
    'menu' => [
        [
            'icon' => 'la la-clipboard',
            'title' => 'Content',
            'route' => '#content',
            'submenu' => [
                [
                    'icon' => 'la la-sitemap',
                    'title' => 'Menus',
                    'route' => '/'.env('ADMIN_URL').'/menu/index'
                ]
            ]
        ]
    ]
];
