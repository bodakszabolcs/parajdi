<?php

namespace Modules\Menu\Transformers;

use App\Http\Resources\BaseResource;

class MenuResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'items' => ItemResource::collection($this->items()->orderBy('order','ASC')->whereNull('parent_id')->get())
        ];
    }
}
