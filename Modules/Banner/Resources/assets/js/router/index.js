import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Banner from '../components/Banner'
import BannerList from '../components/BannerList'
import BannerCreate from '../components/BannerCreate'
import BannerEdit from '../components/BannerEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'banner',
                component: Banner,
                meta: {
                    title: 'Banners'
                },
                children: [
                    {
                        path: 'index',
                        name: 'BannerList',
                        component: BannerList,
                        meta: {
                            title: 'Banners',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/banner/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/banner/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'BannerCreate',
                        component: BannerCreate,
                        meta: {
                            title: 'Create Banners',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/banner/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'BannerEdit',
                        component: BannerEdit,
                        meta: {
                            title: 'Edit Banners',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/banner/index'
                        }
                    }
                ]
            }
        ]
    }
]
