<?php

namespace Modules\Banner\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseBanner extends BaseModel
{
    

    protected $table = 'banners';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'slug',
                    'title' => 'Slug',
                    'type' => 'text',
                    ],[
                    'name' => 'title',
                    'title' => 'Title',
                    'type' => 'text',
                    ],[
                    'name' => 'image',
                    'title' => 'image',
                    'type' => 'text',
                    ],[
                    'name' => 'link',
                    'title' => 'Link',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['slug','title','image','link'];

    protected $casts = [];

    
}
