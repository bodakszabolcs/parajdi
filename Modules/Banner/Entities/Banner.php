<?php

namespace Modules\Banner\Entities;

use Modules\Banner\Entities\Base\BaseBanner;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Banner extends BaseBanner
{
    use SoftDeletes, Cachable, HasTranslations;





    public $translatable = ["title"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
