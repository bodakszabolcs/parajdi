<?php

namespace Modules\Banner\Observers;

use Modules\Banner\Entities\Banner;

class BannerObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function saved(Banner $model)
    {
        Banner::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function created(Banner $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function updated(Banner $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function deleted(Banner $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function restored(Banner $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Banner\Entities\Banner  $model
     * @return void
     */
    public function forceDeleted(Banner $model)
    {
        //
    }
}
