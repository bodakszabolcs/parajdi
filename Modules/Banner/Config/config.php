<?php

return [
    'name' => 'Banner',

                 'menu_order' => 22,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Banner',

                      'route' =>'/'.env('ADMIN_URL').'/banner/index',

                     ]

                 ]
];
