<?php

namespace Modules\Banner\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class BannerViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "slug" => $this->slug,
		    "title" => $this->getTranslatable("title"),
		    "image" => $this->image,
		    "link" => $this->link,
		"selectables" => [
		]
		     ];
    }
}
