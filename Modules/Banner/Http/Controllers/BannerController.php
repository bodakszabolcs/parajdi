<?php

namespace Modules\Banner\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Banner\Entities\Banner;
use Illuminate\Http\Request;
use Modules\Banner\Http\Requests\BannerCreateRequest;
use Modules\Banner\Http\Requests\BannerUpdateRequest;
use Modules\Banner\Transformers\BannerViewResource;
use Modules\Banner\Transformers\BannerListResource;

class BannerController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Banner();
        $this->viewResource = BannerViewResource::class;
        $this->listResource = BannerListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = BannerCreateRequest::class;
        $this->updateRequest = BannerUpdateRequest::class;
    }

}
