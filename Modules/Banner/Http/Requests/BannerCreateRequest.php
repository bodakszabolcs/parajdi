<?php

namespace Modules\Banner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required',
			'title' => 'required',
			'image' => 'required',
			'link' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'slug' => __('Slug'),
'title' => __('Title'),
'image' => __('image'),
'link' => __('Link'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
