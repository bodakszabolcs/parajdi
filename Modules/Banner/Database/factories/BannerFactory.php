<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Banner\Entities\Banner;

$factory->define(Banner::class, function (Faker $faker) {
    return [
        "slug" => $faker->slug(),
        "title" => $faker->realText(),
        "image" => 'https://source.unsplash.com/1600x900/?brand'.rand(0,9999),
        "link" => $faker->imageUrl(),

    ];
});
