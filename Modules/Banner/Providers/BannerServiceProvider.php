<?php

namespace Modules\Banner\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Banner\Entities\Banner;
use Modules\Banner\Observers\BannerObserver;

class BannerServiceProvider extends ModuleServiceProvider
{
    protected $module = 'banner';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Banner::observe(BannerObserver::class);
    }
}
