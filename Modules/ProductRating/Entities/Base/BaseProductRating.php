<?php

namespace Modules\ProductRating\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Webshop\Entities\Product;


abstract class BaseProductRating extends BaseModel
{


    protected $table = 'product_ratings';

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'not_useful',
                    'title' => 'Not useful',
                    'type' => 'text',
                    ],[
                    'name' => 'email',
                    'title' => 'Email',
                    'type' => 'text',
                    ],[
                    'name' => 'name',
                    'title' => 'Name',
                    'type' => 'text',
                    ],[
                    'name' => 'title',
                    'title' => 'Title',
                    'type' => 'text',
                    ],[
                    'name' => 'comment',
                    'title' => 'Comment',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }


    protected $fillable = ['product_id','rate','moderated','useful','not_useful','email','name','title','comment'];

    protected $casts = [];

     public function product()
    {
        return $this->belongsTo('Modules\Product\Entities\Product','product_id','id');
    }



}
