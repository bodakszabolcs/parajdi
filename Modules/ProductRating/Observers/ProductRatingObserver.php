<?php

namespace Modules\ProductRating\Observers;

use Modules\ProductRating\Entities\ProductRating;

class ProductRatingObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function saved(ProductRating $model)
    {
        ProductRating::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function created(ProductRating $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function updated(ProductRating $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function deleted(ProductRating $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function restored(ProductRating $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProductRating\Entities\ProductRating  $model
     * @return void
     */
    public function forceDeleted(ProductRating $model)
    {
        //
    }
}
