<?php

namespace Modules\ProductRating\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRatingCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
			'rate' => 'required',
			'email' => 'required',
			'name' => 'required',
			'title' => 'required',
			'comment' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'product_id' => __('Product'),
'rate' => __('Rating'),
'moderated' => __('Moderated'),
'useful' => __('Useful'),
'not_useful' => __('Not useful'),
'email' => __('Email'),
'name' => __('Name'),
'title' => __('Title'),
'comment' => __('Comment'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
