<?php

namespace Modules\ProductRating\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProductRating\Entities\ProductRating;
use Illuminate\Http\Request;
use Modules\ProductRating\Http\Requests\ProductRatingCreateRequest;
use Modules\ProductRating\Http\Requests\ProductRatingUpdateRequest;
use Modules\ProductRating\Transformers\ProductRatingViewResource;
use Modules\ProductRating\Transformers\ProductRatingListResource;

class ProductRatingController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProductRating();
        $this->viewResource = ProductRatingViewResource::class;
        $this->listResource = ProductRatingListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProductRatingCreateRequest::class;
        $this->updateRequest = ProductRatingUpdateRequest::class;
    }

}
