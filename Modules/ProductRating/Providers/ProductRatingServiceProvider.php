<?php

namespace Modules\ProductRating\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProductRating\Entities\ProductRating;
use Modules\ProductRating\Observers\ProductRatingObserver;

class ProductRatingServiceProvider extends ModuleServiceProvider
{
    protected $module = 'productrating';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProductRating::observe(ProductRatingObserver::class);
    }
}
