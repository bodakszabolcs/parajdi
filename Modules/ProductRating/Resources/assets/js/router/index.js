import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProductRating from '../components/ProductRating'
import ProductRatingList from '../components/ProductRatingList'
import ProductRatingCreate from '../components/ProductRatingCreate'
import ProductRatingEdit from '../components/ProductRatingEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'productrating',
                component: ProductRating,
                meta: {
                    title: 'ProductRatings'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProductRatingList',
                        component: ProductRatingList,
                        meta: {
                            title: 'ProductRatings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productrating/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProductRatingCreate',
                        component: ProductRatingCreate,
                        meta: {
                            title: 'Create ProductRatings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productrating/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProductRatingEdit',
                        component: ProductRatingEdit,
                        meta: {
                            title: 'Edit ProductRatings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/productrating/index'
                        }
                    }
                ]
            }
        ]
    }
]
