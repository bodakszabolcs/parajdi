<?php
    /* @var $factory \Illuminate\Database\Eloquent\Factory */
    use Faker\Generator as Faker;
    use Modules\ProductRating\Entities\ProductRating;
    $factory->define(ProductRating::class, function (Faker $faker) {
        return [
            "product_id" => rand(1,300),
            "rate" => rand(1,5),
            "moderated" => 1,
            "useful" => rand(1,50),
            "not_useful" => rand(1,50),
            "email" => $faker->email(),
            "name" => $faker->name(),
            "title" => $faker->word(),
            "comment" => $faker->realText(),
        ];
    });
