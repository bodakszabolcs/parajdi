<?php

namespace Modules\ProductRating\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Product\Entities\Product;
use Modules\Product\Transformers\ProductListResource;
class ProductRatingViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "product_id" => $this->product_id,
            "product" => new ProductListResource($this->product),
		    "rate" => $this->rate,
		    "moderated" => $this->moderated,
		    "useful" => $this->useful,
		    "not_useful" => $this->not_useful,
		    "email" => $this->email,
		    "name" => $this->name,
		    "title" => $this->title,
		    "comment" => $this->comment,
		"selectables" => [
		"product" => Product::pluck("name","id"),
		]
		     ];
    }
}
