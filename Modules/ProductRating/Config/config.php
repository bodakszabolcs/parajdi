<?php

return [
    'name' => 'ProductRating',

    'menu_order' => 20,

    'menu' => [
        [
            'icon' => 'la la-newspaper-o',
            'title' => 'Catalog',
            'route' => '#catalog',
            'submenu' => [
                [

                    'icon' => 'la la-star-half-o',

                    'title' => 'Product ratings',

                    'route' => '/' . env('ADMIN_URL') . '/productrating/index',

                ]
            ]
        ]
    ]
];
