<?php

return [
    'name' => 'Admin',
    'menu_order' => 1,
    'menu' => [
        [
            'icon' => 'la la-dashboard',
            'title' => 'Dashboard',
            'route' => '/'.env('ADMIN_URL')
        ]
    ]
];
