<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;
use Modules\Webshop\Entities\Currency;
use Modules\Webshop\Entities\CurrencyExchange;
use Modules\Webshop\Entities\Order;
use Modules\Webshop\Entities\OrderItem;
use Modules\Webshop\Entities\ProductStock;

class AdminController extends AbstractLiquidController
{
    public function notifications(Request $request)
    {
        $user = Auth::user();

        return response()->json($user->unreadNotifications, $this->successStatus);
    }

    public function dashboard(Request $request)
    {
        if (Cache::has('dashboard'))
        {
            return response()->json(Cache::get('dashboard'), $this->successStatus);
        }

        $tp = [];

        $currencies = Currency::all();

        foreach ($currencies as $currency) {
            $totalProfit = Order::whereHas('billingData', function ($query) {
                $query->having(DB::raw('COUNT(*)'), '=', 1);
            })
                ->where('currency_id', '=', $currency->id)
                ->groupBy('ws_orders.id')->sum('order_total');

            $tp[$currency->id] = price_format($totalProfit, $currency->id);
        }

        $newOrders = Order::where('created_at', '>', now()->add('-1 day'))->count();
        $lastOrders = Order::where('created_at', '>', now()->add('-2 day'))->where('created_at', '<',
            now()->add('-1 day'))->count();

        $newUsers = User::where('created_at', '>', now()->add('-1 day'))->count();
        $lastUsers = User::where('created_at', '>', now()->add('-2 day'))->where('created_at', '<',
            now()->add('-1 day'))->count();

        $stocks = ProductStock::sum('sum_quantity');

        $todayNewUsers = User::select('id', 'name', 'avatar', 'email')->orderBy('created_at', 'DESC')->limit(10)->get();
        $monthlyNewUsers = User::select('id', 'name', 'avatar', 'email')->orderBy('created_at',
            'DESC')->whereMonth('created_at', date("m"))->limit(30)->get();

        $latestBest = OrderItem::with(['variation', 'order'])->whereHas('order', function ($query) {
            $query->where('created_at', '>', now()->add('- 7 days'));
        })->select('*', DB::raw('COUNT(*) as cnt'))->orderBy('cnt', 'DESC')->groupBy('variation_id')->limit(3)->get();
        $latestBestArr = [];
        $index = 0;
        foreach ($latestBest as $lb) {
            $latestBestArr[$index] = $lb;
            $latestBestArr[$index]['variation'] = $lb->variation;
            $latestBestArr[$index]['stock'] = $lb->variation->stocks;
            $index++;
        }

        $monthlyBest = OrderItem::with(['variation', 'order'])->whereHas('order', function ($query) {
            $query->whereMonth('created_at', date("m"));
        })->select('*', DB::raw('COUNT(*) as cnt'))->orderBy('cnt', 'DESC')->groupBy('variation_id')->limit(3)->get();
        $monthlyBestArr = [];
        $index = 0;
        foreach ($monthlyBest as $lb) {
            $monthlyBestArr[$index] = $lb;
            $monthlyBestArr[$index]['variation'] = $lb->variation;
            $monthlyBestArr[$index]['stock'] = $lb->variation->stocks;
            $index++;
        }

        $allTimeBest = OrderItem::with(['variation'])->select('*', DB::raw('COUNT(*) as cnt'))->orderBy('cnt',
            'DESC')->groupBy('variation_id')->limit(3)->get();
        $allTimeBestArr = [];
        $index = 0;
        foreach ($allTimeBest as $lb) {
            $allTimeBestArr[$index] = $lb;
            $allTimeBestArr[$index]['variation'] = $lb->variation;
            $allTimeBestArr[$index]['stock'] = $lb->variation->stocks;
            $index++;
        }

        $chartDataOrders = DB::table('ws_orders')->select(DB::raw('CONCAT(YEAR(created_at)," ",MONTHNAME(created_at)) as Month'),
            DB::raw('COUNT(created_at) as DateCount'))
            ->groupBy([DB::raw('MONTH(created_at)'), DB::raw('MONTHNAME(created_at)')])
            ->orderBy('created_at', 'ASC')
            ->where('created_at', '>', now()->add('- 1 year'))
            ->get()
            ->toArray();

        $chartDataUsers = DB::table('users')->select(DB::raw('CONCAT(YEAR(created_at)," ",MONTHNAME(created_at)) as Month'),
            DB::raw('COUNT(created_at) as DateCount'))
            ->groupBy([DB::raw('MONTH(created_at)'), DB::raw('MONTHNAME(created_at)')])
            ->orderBy('created_at', 'ASC')
            ->where('created_at', '>', now()->add('- 1 year'))
            ->get()
            ->toArray();

        $labels = [];
        foreach ($chartDataOrders as $cd) {
            $labels[] = $cd->Month;
        }

        $co = array_fill(0, sizeof($labels), 0);
        $cu = array_fill(0, sizeof($labels), 0);

        $index = 0;
        foreach ($labels as $l) {
            foreach ($chartDataOrders as $cdo) {
                if ($cdo->Month == $l) {
                    $co[$index] = $cdo->DateCount;
                }
            }
            foreach ($chartDataUsers as $cdo) {
                if ($cdo->Month == $l) {
                    $cu[$index] = $cdo->DateCount;
                }
            }
            $index++;
        }

        $four_stat = [
            'tp' => $tp,
            'new_orders' => $newOrders,
            'change_orders' => ($lastOrders !== 0) ? (round($newOrders / $lastOrders * 100, 2)) : 100,
            'new_users' => $newUsers,
            'change_users' => ($lastUsers !== 0) ? (round($newUsers / $lastUsers * 100, 2)) : 100,
            'stocks' => $stocks
        ];

        $new_items = [
            'today_new_users' => (!empty($todayNewUsers)) ? $todayNewUsers : [],
            'monthly_new_users' => (!empty($monthlyNewUsers)) ? $monthlyNewUsers : []
        ];

        $best_sellers = [
            'latest' => $latestBestArr,
            'monthly' => $monthlyBestArr,
            'all_time' => $allTimeBest
        ];

        $chart = [
            'orders' => $co,
            'users' => $cu,
            'labels' => $labels
        ];

        Cache::put('dashboard',[
            'four_stat' => $four_stat,
            'new_items' => $new_items,
            'best_sellers' => $best_sellers,
            'chart' => $chart
        ],now()->addMinutes(30));

        return response()->json([
            'four_stat' => $four_stat,
            'new_items' => $new_items,
            'best_sellers' => $best_sellers,
            'chart' => $chart
        ], $this->successStatus);
    }


    public function getMenu(Request $request)
    {
        $menus = parent::getMenu($request);
        $isSuperAdmin = Auth::user()->isSuperAdmin();

        $superAdminRoutes = ['/' . config('app.admin_url') . '/module-builder/index', '/telescope'];
        $productionRoutes = (config('app.env') === 'production') ? ['/' . config('app.admin_url') . '/module-builder/index'] : [];

        if ($isSuperAdmin) {
            return $menus;
        }

        $mergedRoles = [];
        $roles = Auth::user()->roles;

        foreach ($roles as $r) {
            try {
                $mergedRoles = array_merge($mergedRoles, json_decode($r->menu, true));
            } catch (\Exception $e) {
                $mergedRoles = array_merge($mergedRoles, $r->menu);
            }
        }

        $index = 0;
        foreach ($menus as $m) {
            if (!array_key_exists($m['route'], $mergedRoles)) {
                unset($menus[$index]);
            } else {
                if (in_array($m['route'], $superAdminRoutes) && in_array($m['route'],
                        $productionRoutes) && !$isSuperAdmin) {
                    unset($menus[$index]);
                }
            }

            if (isset($m['submenu'])) {
                $kndex = 0;
                foreach ($m['submenu'] as $k) {
                    if (!array_key_exists($k['route'], $mergedRoles)) {
                        unset($menus[$index]['submenu'][$kndex]);
                    } else {
                        if (in_array($k['route'], $superAdminRoutes) && in_array($k['route'],
                                $productionRoutes) && !$isSuperAdmin) {
                            unset($menus[$index]);
                        }
                    }
                    $kndex++;
                }
            }
            $index++;
        }

        return $menus;
    }

    public function uploadFile(Request $request)
    {

        $file = $request->file->store('public/files/shares'.$request->input('dir'), 'public');

        $fileExpl = explode("public/", $file);

        return \response()->json(['path' => '/storage/public/' . end($fileExpl), 'url' => url('storage/' . end($fileExpl))],
            $this->successStatus);
    }
}
