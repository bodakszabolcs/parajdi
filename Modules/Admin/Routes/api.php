<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/notifications', 'AdminController@notifications');
    Route::get('/menu', 'AdminController@getMenu');

    Route::get('/dashboard', 'AdminController@dashboard')->name('Dashboard');

    Route::post('/upload-file', 'AdminController@uploadFile')->name('Upload file');
});
