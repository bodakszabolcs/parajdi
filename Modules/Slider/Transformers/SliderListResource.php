<?php

namespace Modules\Slider\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class SliderListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "lead" => $this->lead,
		    "link" => $this->link,
		    "order" => $this->order,
		    "button_text" => $this->button_text,
		    "image_path" => $this->image_path,
		     ];
    }
}
