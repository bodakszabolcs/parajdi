<?php

namespace Modules\Slider\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lead' => 'required',
            'button_text' => 'required',
			'link' => 'required',
			'image_path' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Title'),
                'lead' => __('Lead'),
                'button_text' => __('Link button text'),
                'link' => __('Link'),
                'order' => __('Order'),
                'image_path' => __('Image'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
