<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    class CreateSliderTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::dropIfExists('sliders');
            Schema::create('sliders', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->json("name")->nullable();
                $table->json("lead")->nullable();
                $table->json("button_text")->nullable();
                $table->string("link")->nullable();
                $table->tinyInteger("order")->unsigned()->nullable();
                $table->string("image_path")->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(["deleted_at"]);

            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('sliders');
        }
    }
