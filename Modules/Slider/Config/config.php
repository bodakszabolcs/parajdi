<?php

return [
    'name' => 'Slider',

    'menu_order' => 16,

    'menu' => [
        [
            'icon' => 'la la-clipboard',
            'title' => 'Content',
            'route' => '#content',
            'submenu' => [
                [
                    'icon' => 'flaticon2-website',
                    'title' => 'Slider',
                    'route' => '/'.env('ADMIN_URL').'/sliders/index'
                ],
                [
                    'icon' => 'flaticon2-website',
                    'title' => 'Brand',
                    'route' => '/'.env('ADMIN_URL').'/brands/index'
                ]

            ]
        ]
    ]
];
