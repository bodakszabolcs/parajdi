import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Type from '../components/Type'
import TypeList from '../components/TypeList'
import TypeCreate from '../components/TypeCreate'
import TypeEdit from '../components/TypeEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'type',
                component: Type,
                meta: {
                    title: 'Types'
                },
                children: [
                    {
                        path: 'index',
                        name: 'TypeList',
                        component: TypeList,
                        meta: {
                            title: 'Types',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/type/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/type/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'TypeCreate',
                        component: TypeCreate,
                        meta: {
                            title: 'Create Types',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/type/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'TypeEdit',
                        component: TypeEdit,
                        meta: {
                            title: 'Edit Types',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/type/index'
                        }
                    }
                ]
            }
        ]
    }
]
