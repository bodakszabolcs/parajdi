<?php

namespace Modules\Type\Observers;

use Modules\Type\Entities\Type;

class TypeObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function saved(Type $model)
    {
        Type::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function created(Type $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function updated(Type $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function deleted(Type $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function restored(Type $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Type\Entities\Type  $model
     * @return void
     */
    public function forceDeleted(Type $model)
    {
        //
    }
}
