<?php

namespace Modules\Type\Entities;

use Modules\Type\Entities\Base\BaseType;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Type extends BaseType
{
    use SoftDeletes, Cachable, HasTranslations;

    public $translatable = ["name"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
