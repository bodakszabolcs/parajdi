<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Type\Entities\Type;

$factory->define(Type::class, function (Faker $faker) {
    return [
        "name" => $faker->word(),
    ];
});
