<?php

namespace Modules\Type\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Type\Entities\Type;
use Illuminate\Http\Request;
use Modules\Type\Http\Requests\TypeCreateRequest;
use Modules\Type\Http\Requests\TypeUpdateRequest;
use Modules\Type\Transformers\TypeViewResource;
use Modules\Type\Transformers\TypeListResource;

class TypeController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Type();
        $this->viewResource = TypeViewResource::class;
        $this->listResource = TypeListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = TypeCreateRequest::class;
        $this->updateRequest = TypeUpdateRequest::class;
    }

}
