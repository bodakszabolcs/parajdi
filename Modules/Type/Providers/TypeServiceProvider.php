<?php

namespace Modules\Type\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Type\Entities\Type;
use Modules\Type\Observers\TypeObserver;

class TypeServiceProvider extends ModuleServiceProvider
{
    protected $module = 'type';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Type::observe(TypeObserver::class);
    }
}
