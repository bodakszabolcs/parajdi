import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Category from '../components/Category/Category'
import CategoryList from '../components/Category/CategoryList'
import CategoryCreate from '../components/Category/CategoryCreate'
import CategoryEdit from '../components/Category/CategoryEdit'
import Tag from '../components/Tag/Tag'
import TagList from '../components/Tag/TagList'
import TagCreate from '../components/Tag/TagCreate'
import TagEdit from '../components/Tag/TagEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'category',
                component: Category,
                meta: {
                    title: 'Categories'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CategoryList',
                        component: CategoryList,
                        meta: {
                            title: 'Categories',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/category/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/category/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CategoryCreate',
                        component: CategoryCreate,
                        meta: {
                            title: 'Create Category',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/category/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CategoryEdit',
                        component: CategoryEdit,
                        meta: {
                            title: 'Edit Category',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/category/index'
                        }
                    }
                ]
            },
            {
                path: 'tag',
                component: Tag,
                meta: {
                    title: 'Tags'
                },
                children: [
                    {
                        path: 'index',
                        name: 'TagList',
                        component: TagList,
                        meta: {
                            title: 'Tags',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/tag/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/tag/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'TagCreate',
                        component: TagCreate,
                        meta: {
                            title: 'Create Tag',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/tag/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'TagEdit',
                        component: TagEdit,
                        meta: {
                            title: 'Edit Tag',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/tag/index'
                        }
                    }
                ]
            }
        ]
    }
]
