<?php

namespace Modules\Category\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Modules\Category\Entities\Tag;
use Modules\Category\Http\Requests\TagCreateRequest;
use Modules\Category\Transformers\TagListResource;
use Modules\Category\Transformers\TagViewResource;

class TagController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Tag();
        $this->listResource = TagListResource::class;
        $this->viewResource = TagViewResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = TagCreateRequest::class;
        $this->updateRequest = TagCreateRequest::class;
    }

    public function getTags(Request $request)
    {
        $tags = Tag::where('id','>',0);

        if ($request->input('id',0) != 0) {
            $tags = $tags->where('id','!=',$request->input('id'));
        }

        return response()->json(['data' => $tags->pluck('name','id')], $this->successStatus);
    }
}
