<?php

namespace Modules\Category\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\System\Entities\Settings;

abstract class BaseCategory extends BaseModel
{
    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'slug',
        'meta_title',
        'meta_description',
        'og_image',
        'parent_id'
    ];

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        if (Arr::get($request, 'slug', null) == null) {
            $this->slug = $this->slugify($this->name.' '.$this->id);
        }

        $this->save();

        return $this;

    }

    public function parent()
    {
        return $this->belongsTo('Modules\Category\Entities\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('Modules\Category\Entities\Category', 'parent_id', 'id');
    }

    public function blog()
    {
        return $this->belongsToMany('Modules\Blog\Entities\Blog', 'blog_categories', 'blog_id', 'category_id');
    }

}
