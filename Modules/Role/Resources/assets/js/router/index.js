import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Role from '../components/Role'
import RoleList from '../components/RoleList'
import RoleCreate from '../components/RoleCreate'
import RoleEdit from '../components/RoleEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'role',
                component: Role,
                meta: {
                    title: 'Roles'
                },
                children: [
                    {
                        path: 'index',
                        name: 'RoleList',
                        component: RoleList,
                        meta: {
                            title: 'Roles',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/role/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/role/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'RoleCreate',
                        component: RoleCreate,
                        meta: {
                            title: 'Create Role',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/role/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'RoleEdit',
                        component: RoleEdit,
                        meta: {
                            title: 'Edit Role',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/role/index'
                        }
                    }
                ]
            }
        ]
    }
]
