<?php

namespace Modules\Role\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Role\Entities\Role;
use Modules\Role\Http\Requests\RoleCreateRequest;
use Modules\Role\Transformers\RoleResource;

class RoleController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Role();
        $this->viewResource = RoleResource::class;
        $this->listResource = RoleResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = RoleCreateRequest::class;
        $this->updateRequest = RoleCreateRequest::class;
    }

    public function credentials(Request $request)
    {
        $routes = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            if (isset($route->action["middleware"]) && is_array($route->action["middleware"]) && in_array('role', $route->action["middleware"]))
            {
                $routes[] = $route;
            }
        }

        return response()->json(['routes' => $routes, 'menus' => parent::getMenu($request)], $this->successStatus);
    }

    public function roleList(Request $request)
    {
        return response()->json(Role::all(), $this->successStatus);
    }

    public function destroy($id, $auth = null)
    {
        if ($id == 1) {
            abort(422);
        }
        return parent::destroy($id, $auth);
    }
}
