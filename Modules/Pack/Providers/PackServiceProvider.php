<?php

namespace Modules\Pack\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Pack\Entities\Pack;
use Modules\Pack\Observers\PackObserver;

class PackServiceProvider extends ModuleServiceProvider
{
    protected $module = 'pack';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Pack::observe(PackObserver::class);
    }
}
