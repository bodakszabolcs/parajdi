import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Pack from '../components/Pack'
import PackList from '../components/PackList'
import PackCreate from '../components/PackCreate'
import PackEdit from '../components/PackEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'pack',
                component: Pack,
                meta: {
                    title: 'Packs'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PackList',
                        component: PackList,
                        meta: {
                            title: 'Packs',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/pack/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/pack/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PackCreate',
                        component: PackCreate,
                        meta: {
                            title: 'Create Packs',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/pack/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PackEdit',
                        component: PackEdit,
                        meta: {
                            title: 'Edit Packs',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/pack/index'
                        }
                    }
                ]
            }
        ]
    }
]
