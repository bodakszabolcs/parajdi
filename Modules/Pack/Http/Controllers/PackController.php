<?php

namespace Modules\Pack\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Pack\Entities\Pack;
use Illuminate\Http\Request;
use Modules\Pack\Http\Requests\PackCreateRequest;
use Modules\Pack\Http\Requests\PackUpdateRequest;
use Modules\Pack\Transformers\PackViewResource;
use Modules\Pack\Transformers\PackListResource;

class PackController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Pack();
        $this->viewResource = PackViewResource::class;
        $this->listResource = PackListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PackCreateRequest::class;
        $this->updateRequest = PackUpdateRequest::class;
    }

}
