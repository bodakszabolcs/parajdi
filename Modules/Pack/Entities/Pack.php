<?php

namespace Modules\Pack\Entities;

use Modules\Pack\Entities\Base\BasePack;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Pack extends BasePack
{
    use SoftDeletes, Cachable,HasTranslations;

    public $translatable = ["name"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
