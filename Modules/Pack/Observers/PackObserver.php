<?php

namespace Modules\Pack\Observers;

use Modules\Pack\Entities\Pack;

class PackObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function saved(Pack $model)
    {
        Pack::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function created(Pack $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function updated(Pack $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function deleted(Pack $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function restored(Pack $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Pack\Entities\Pack  $model
     * @return void
     */
    public function forceDeleted(Pack $model)
    {
        //
    }
}
