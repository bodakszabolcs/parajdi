<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Pack\Entities\Pack;

$factory->define(Pack::class, function (Faker $faker) {
    return [
        "name" => rand(0,3).'-'.rand(5,10). "kg",

    ];
});
