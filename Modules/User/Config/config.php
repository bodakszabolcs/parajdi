<?php

return [
    'name' => 'User',
    'menu_order' => 3,
    'menu' => [
        [
            'icon' => 'la la-users',
            'title' => 'Users',
            'route' => '/'.env('ADMIN_URL').'/user/index'
        ]
    ]
];
