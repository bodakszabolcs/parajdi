<?php

namespace Modules\User\Http\Controllers;

use App\AccessLog;
use App\Http\Controllers\AbstractLiquidController;
use App\MailLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\User\Emails\GeneratePassword;
use Modules\User\Entities\User;
use Modules\User\Entities\UserFilter;
use Modules\User\Http\Requests\ChangePasswordRequest;
use Modules\User\Http\Requests\UserCreateRequest;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Transformers\AccessLogResource;
use Modules\User\Transformers\MailLogResource;
use Modules\User\Transformers\UserListResource;
use Modules\User\Transformers\UserViewResource;

class UserController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new User();
        $this->viewResource = UserViewResource::class;
        $this->listResource = UserListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = UserCreateRequest::class;
        $this->updateRequest = UserUpdateRequest::class;
    }

    public function getUser(Request $request)
    {
        return response()->json(new UserViewResource(Auth::user()), $this->successStatus);
    }

    public function destroy($id, $auth = null)
    {
        if ($id == 1) {
            abort(422);
        }
        return parent::destroy($id, $auth);
    }

    public function saveFilter(Request $request)
    {
        $uf = new UserFilter();
        $uf->user_id = Auth::id();
        $uf->path = $request->input('path');
        $uf->name = $request->input('name');
        $uf->params = $request->input('params');
        $uf->query = $request->input('query');
        $uf->save();

        return response()->json(UserFilter::where('user_id', '=', Auth::id())->get(), $this->successStatus);
    }
    public function changePassword(ChangePasswordRequest $request) {
        $user = User::find(Auth::id());

        $user->password = Hash::make($request->input('password'));
        $user->save();

        return parent::show($request, Auth::id());
    }
    public function deleteFilter(Request $request, $id)
    {
        try {
            $uf = UserFilter::where('id', '=', $id)->where('user_id', '=', Auth::id())->firstOrFail();
            $uf->delete();

            return response()->json(UserFilter::where('user_id', '=', Auth::id())->get(), $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }

    public function forceLogin(Request $request, $id)
    {
        try {
            $user = User::where('id', '=', $id)->firstOrFail();

            $user->roles_length = $user->roles()->count();
            $user->api_token = $user->createToken('force-login-'.$user->email.'-'.date("YmdHi"))->plainTextToken;

            return response()->json($user, $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }

    public function update(Request $request, $id, $auth = null)
    {
        if ($request->has('new_password') && $request->input('new_password') != null) {
            $validator = Validator::make($request->all(), [
                'new_password' => 'required|string|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), $this->errorStatus);
            }
        }

        return parent::update($request, $id, $auth);
    }
    public function deleteProfile(Request $request)
    {
        try {
            $user = User::where('id', '=', Auth::user()->id)->where('id','<>',1)->firstOrFail();
            $user->delete();
            return response()->json($this->successMessage, $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors'=>[__('The user cannot be deleted')]], $this->errorStatus);
        }
    }
    public function getProfile(Request $request)
    {
        return parent::show($request, Auth::id());
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        return $this->update($request, Auth::id());
    }

    public function accessLogs(Request $request, $id = 0)
    {
        $access = AccessLog::where('id', '>', 0);

        if ($id != 0) {
            $access = $access->where('user_id', '=', $id);
        }

        $access = $access->orderBy('id', 'DESC')->paginate();

        return AccessLogResource::collection($access);
    }

    public function mailLogs(Request $request, $id = 0)
    {
        $mail = MailLog::where('id', '>', 0);

        if ($id != 0) {
            $user = User::find($id);

            $mail = $mail->where('to', '=', optional($user)->email);
        }

        $mail = $mail->orderBy('id', 'DESC')->paginate();

        return MailLogResource::collection($mail);
    }

    public function accessLogsProfile(Request $request)
    {
        return $this->accessLogs($request, Auth::id());
    }

    public function mailLogsProfile(Request $request)
    {
        return $this->mailLogs($request, Auth::id());
    }

    public function generatePassword(Request $request, $id)
    {
        try {
            $user = User::where('id','=',$id)->firstOrFail();

            $pass = Str::random(12);
            $user->password = Hash::make($pass);
            $user->save();

            Mail::to($user)->locale(App::getLocale())->send(new GeneratePassword($user, $pass));

            return response()->json(['status' => 'OK'], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }
}
