<?php

namespace Modules\User\Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testCreateEmptyUser()
    {
        try {
            $user = new User();
            $user->email = null;
            $user->save();

            $this->assertTrue(false);
        } catch (QueryException $e) {
            $this->assertTrue(true);
        }
    }

    public function testCreateValidUser()
    {
        $user = new User();
        $user->name = 'Tester Ester';
        $user->email = 'tester1@test.com';
        $user->password = Hash::make('aA123456');
        $user->save();

        $this->assertDatabaseHas('users',[
            'email' => 'tester1@test.com'
        ]);
    }

    public function testUniqueEmailValidation()
    {
        try {
            $user = new User();
            $user->name = 'Tester Ester';
            $user->email = 'tester1@test.com';
            $user->password = Hash::make('aA123456');
            $user->save();

            $this->assertTrue(false);
        } catch (QueryException $e) {
            $this->assertTrue(true);
        }
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
