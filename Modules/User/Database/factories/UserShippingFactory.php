<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\User\Entities\UserShipping;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(UserShipping::class, function (Faker $faker) {
    return [
        'country_id' => null,
        'zip' => $faker->numberBetween(1000,7000),
        'name' => $faker->firstName,
        'phone' => $faker->phoneNumber,
        'user_id' => 1
    ];
});
