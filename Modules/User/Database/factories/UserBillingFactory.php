<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\User\Entities\UserBilling;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(UserBilling::class, function (Faker $faker) {
    return [
        'country_id' => null,
        'zip' => $faker->numberBetween(1000,7000),


        'name' => $faker->lastName,
        'company_name' => $faker->company,
        'vat_number' => $faker->numberBetween(10000,80000),
        'user_id' => 1
    ];
});
