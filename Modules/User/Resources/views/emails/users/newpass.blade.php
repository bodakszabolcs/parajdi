@component('mail::message')
# {{__('New password')}}

{{__('Your new password')}}: {{$pass}}

{{__('Thanks')}},<br>
{{ config('app.name') }}
@endcomponent
