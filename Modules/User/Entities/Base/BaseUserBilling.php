<?php

namespace Modules\User\Entities\Base;

use App\BaseModel;

abstract class BaseUserBilling extends BaseModel
{
    protected $table = 'user_billing';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'firstname',
        'lastname',
        'company_name',
        'country_id',
        'county',
        'zip',
        'city',
        'phone',
        'area',
        'area_type',
        'building',
        'level',
        'door',
        'stairs',
        'vat_number'
    ];

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id', 'id');
    }

}
