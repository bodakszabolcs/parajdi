<?php

namespace Modules\User\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\Base\BaseUserBilling;

class UserBilling extends BaseUserBilling
{
    use Cachable, SoftDeletes;
}
