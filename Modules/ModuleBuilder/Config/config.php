<?php

return [
    'name' => 'ModuleBuilder',
    'menu_order' => 998,
    'menu' => [
        [
            'icon' => 'la la-gear',
            'title' => 'System',
            'route' => '#system',
            'submenu' => [
                [
                    'icon' => 'la la-legal',
                    'title' => 'Module Builder',
                    'route' => '/'.env('ADMIN_URL').'/module-builder/index'
                ]
            ]
        ]
    ],
    'middlewares' => [
        'auth:sanctum' => 'auth:sanctum',
        'role' => 'role',
        'accesslog' => 'accesslog'
    ],
];
