<?php

namespace Modules\ModuleBuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEntityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moduleName' => 'required',
            'model' => 'required',
            'collection' => 'required',
            'options' => 'array',
            'options.fillable' => 'array',
            'options.searchColumns' => 'array',
            'options.searchColumnLabels' => 'array',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
