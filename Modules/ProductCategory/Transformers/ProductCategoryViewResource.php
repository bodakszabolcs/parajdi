<?php

namespace Modules\ProductCategory\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProductCategoryViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->getTranslatable('name'),
		    "description" => $this->getTranslatable('description'),
		    "image" => $this->image,
		    "parent_id" => $this->parent_id,
		    "slug" => $this->slug,
            'children' => ProductCategoryViewResource::collection($this->children)

		     ];
    }
}
