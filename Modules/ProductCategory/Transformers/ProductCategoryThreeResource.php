<?php



    namespace Modules\ProductCategory\Transformers;
    use App\Http\Resources\BaseResource;
    class ProductCategoryThreeResource extends BaseResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'slug' => $this->slug,
                'description' => $this->description,
                'image' => $this->image,
                'children' => ProductCategoryThreeResource::collection($this->children)
            ];
        }
    }

