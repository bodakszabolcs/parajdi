<?php

namespace Modules\ProductCategory\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Category\Transformers\CategoryListResource;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Category\Transformers\CategoryViewResource;
use Modules\ProductCategory\Entities\ProductCategory;
use Illuminate\Http\Request;
use Modules\ProductCategory\Http\Requests\ProductCategoryCreateRequest;
use Modules\ProductCategory\Http\Requests\ProductCategoryUpdateRequest;
use Modules\ProductCategory\Transformers\ProductCategoryViewResource;
use Modules\ProductCategory\Transformers\ProductCategoryListResource;

class ProductCategoryController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProductCategory();
        $this->viewResource = ProductCategoryViewResource::class;
        $this->listResource = ProductCategoryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProductCategoryCreateRequest::class;
        $this->updateRequest = ProductCategoryUpdateRequest::class;
    }
    public function getCategories(Request $request)
    {
        $category =ProductCategory::where('id','>',0);

        if ($request->input('id',0) != 0) {
            $category = $category->where('id','!=',$request->input('id'));
        }

        return response()->json(['data' => $category->pluck('name','id')], $this->successStatus);
    }

    public function getCategoryTree(Request $request)
    {
        return CategoryTreeResource::collection(ProductCategory::whereNull('parent_id')->get());
    }

    public function destroy($id, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrfail();
            }

            $this->model->where('parent_id','=',$this->model->id)->update(['parent_id' => $this->model->parent_id]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }
}
