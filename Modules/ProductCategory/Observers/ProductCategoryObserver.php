<?php

namespace Modules\ProductCategory\Observers;

use Modules\ProductCategory\Entities\ProductCategory;

class ProductCategoryObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function saved(ProductCategory $model)
    {
        ProductCategory::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function created(ProductCategory $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function updated(ProductCategory $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function deleted(ProductCategory $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function restored(ProductCategory $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProductCategory\Entities\ProductCategory  $model
     * @return void
     */
    public function forceDeleted(ProductCategory $model)
    {
        //
    }
}
