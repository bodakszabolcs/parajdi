<?php

namespace Modules\ProductCategory\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProductCategory\Entities\ProductCategory;
use Modules\ProductCategory\Observers\ProductCategoryObserver;

class ProductCategoryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'productcategory';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProductCategory::observe(ProductCategoryObserver::class);
    }
}
