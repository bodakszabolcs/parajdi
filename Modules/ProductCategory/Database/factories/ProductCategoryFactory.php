<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\ProductCategory\Entities\ProductCategory;

$factory->define(ProductCategory::class, function (Faker $faker) {
    $cat =ProductCategory::inRandomOrder()->disableCache()->first();
    $name = $faker->word();
    return [
        "name" => $name,
        "description" => $faker->realText(),
        "slug" => \Illuminate\Support\Str::slug($name),
        "image" => 'https://source.unsplash.com/800x450/?category'.rand(0,9999),
        "parent_id" => (rand(0,1))?(($cat)?$cat->id:null):null,
    ];
});
