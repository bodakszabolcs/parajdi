<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    class CreateProductCategoryTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::dropIfExists('prod_categories');
            Schema::create('prod_categories', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->json("name")->nullable();
                $table->json("description")->nullable();
                $table->text("image")->nullable();
                $table->integer("parent_id")->unsigned()->nullable();
                $table->text("slug")->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(["deleted_at"]);

            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('prod_categories');
        }
    }
