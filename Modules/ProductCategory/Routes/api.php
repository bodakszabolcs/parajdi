<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::group([
        'namespace' => 'Modules\ProductCategory\Http\Controllers',
        'prefix' => '/productcategory',
        'middleware' => ['auth:sanctum']
    ], function () {
        Route::get( '/all', 'ProductCategoryController@getCategories')->name('Get all product categories');
        Route::get( '/tree', 'ProductCategoryController@getCategoryTree')->name('Get product categories tree');
    });
