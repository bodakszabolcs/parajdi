import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProductCategory from '../components/ProductCategory'
import ProductCategoryList from '../components/ProductCategoryList'
import ProductCategoryCreate from '../components/ProductCategoryCreate'
import ProductCategoryEdit from '../components/ProductCategoryEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'product-category',
                component: ProductCategory,
                meta: {
                    title: 'ProductCategories'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProductCategoryList',
                        component: ProductCategoryList,
                        meta: {
                            title: 'ProductCategories',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/product-category/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-category/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProductCategoryCreate',
                        component: ProductCategoryCreate,
                        meta: {
                            title: 'Create ProductCategories',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-category/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProductCategoryEdit',
                        component: ProductCategoryEdit,
                        meta: {
                            title: 'Edit ProductCategories',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/product-category/index'
                        }
                    }
                ]
            }
        ]
    }
]
