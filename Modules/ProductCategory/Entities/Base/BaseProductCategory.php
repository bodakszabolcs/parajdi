<?php

namespace Modules\ProductCategory\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Arr;
use Spatie\Translatable\HasTranslations;


abstract class BaseProductCategory extends BaseModel
{


    protected $table = 'prod_categories';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Name',
                    'type' => 'text',
                    ],[
                    'name' => 'description',
                    'title' => 'Description',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','description','image','parent_id'];

    protected $casts = [];
    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        if (Arr::get($request, 'slug', null) == null) {
            $this->slug = $this->slugify($this->name.' '.$this->id);
        }

        $this->save();

        return $this;

    }

    public function parent()
    {
        return $this->belongsTo('Modules\ProductCategory\Entities\ProductCategory', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('Modules\ProductCategory\Entities\ProductCategory', 'parent_id', 'id');
    }

}
