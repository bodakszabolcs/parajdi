<?php

namespace Modules\ProductCategory\Entities;

use Modules\ProductCategory\Entities\Base\BaseProductCategory;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class ProductCategory extends BaseProductCategory
{
    use SoftDeletes, Cachable, HasTranslations;





    public $translatable = ["name","description"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
