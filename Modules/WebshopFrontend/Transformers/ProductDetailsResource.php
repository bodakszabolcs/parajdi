<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Modules\Category\Transformers\CategoryListResource;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Category\Transformers\CategoryViewResource;
use Modules\Color\Entities\Color;
use Modules\Color\Transformers\ColorListResource;
use Modules\Pack\Entities\Pack;
use Modules\Pack\Transformers\PackListResource;
use Modules\Type\Entities\Type;
use Modules\Type\Transformers\TypeListResource;
use Modules\Webshop\Entities\Attribute;
use Modules\Webshop\Entities\AttributeValue;
use Modules\Webshop\Entities\ProductCategory;
use Modules\Webshop\Entities\ProductStock;
use Modules\Webshop\Entities\Stock;
use Modules\Webshop\Transformers\Attribute\AttributeListResource;
use Modules\Webshop\Transformers\Attribute\AttributeViewResource;
use Modules\Webshop\Transformers\Product\ProductViewResource;


class ProductDetailsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "name"=> $this->name,
            "slug"=> $this->slug,
            "rating_count" => $this->rating_count,
            "rating_avg" => $this->avg_rate,
            "description" =>$this->description,
            'questions' => ProductQuestionResource::collection($this->getQuestions()),
            'ratings' => ProductRatingResource::collection($this->getRatings()),
            'ratingTableData' => $this->getRatingTable(),
            "categories" => CategoryListResource::collection($this->categories),
            "variations" => ProductVariationDetailsResource::collection($this->variations()->where('active','=',1)->where('in_stock','>',0)->orderBy('price','asc')->get()),
            "selectables" => [
                "colors" => ColorListResource::collection(Color::all()),
                "packs"  => PackListResource::collection(Pack::all()),
                "types"  => TypeListResource::collection(Type::all())
            ]

        ];
    }

}
