<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\ProductVariation\Entities\ProductVariation;
class CartItemResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id, 'quantity' => $this->quantity,
            'variation_total' => $this->quantity * $this->variation->getPrice(), 'price' => $this->variation->price,
            'price_default' => $this->variation->price, 'variation' => $this->variation,
            'variation_name' => $this->variation->product->name . ' ' . $this->locale($this->variation->color) . ', ' . $this->locale($this->variation->pack) . ', ' . $this->locale($this->variation->type),
            'product' => new \Modules\Product\Transformers\ProductListResource($this->variation->product)
        ];
    }
    public function locale($data){
        return json_decode($data,1)[App::getLocale()];
    }
}
