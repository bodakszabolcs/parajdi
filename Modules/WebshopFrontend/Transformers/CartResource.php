<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;


class CartResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    private $discount = 0;
    public function toArray($request)
    {
        $shipping = $this->calculateShipping();
        $total = $this->cartTotal(CartItemResource::collection($this->items));
        $subtotal = $this->subTotal(CartItemResource::collection($this->items));
        return [
            'id' => $this->id,
            'uid' => $this->user_id,
            'uuid' => $this->uuid,
            'count' => $this->items()->count(),
            'coupon' => $this->coupons(),
            'total' => $subtotal,
            'status' => $this->status,
            'payment' => $this->payment_status,
            'discount' => $this->discount,
            'shipping_price' => $shipping,
            'total_with_shipping' => $total+$shipping,
            'items' => CartItemResource::collection($this->items)
        ];
    }

    public function cartTotal($items)
    {
        $total = 0;
        foreach ($items as $variation) {
            $var = json_decode(json_encode($variation),1);
            $total +=  $var['variation_total'];
        }
        if($this->coupons() && $this->coupons()->type!= 3){
            if($this->coupons()->type== 1){
                $this->discount = $this->coupons()->value;
                return $total - $this->coupons()->value;
            }
            if($this->coupons()->type== 2){
                $this->discount =  ($total * ($this->coupons()->value/100));
                return $total - ($total * ($this->coupons()->value/100));
            }
        }
        return $total;
    }
    public function subTotal($items){
        $total = 0;
        foreach ($items as $variation) {
            $var = json_decode(json_encode($variation),1);
            $total +=  $var['variation_total'];
        }
        return $total;
    }
}
