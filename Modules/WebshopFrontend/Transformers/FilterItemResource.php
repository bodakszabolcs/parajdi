<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;


class FilterItemResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'class' => $this->class,
            'col_slug' => $this->colSlug,
            'filter_type' => $this->filterType,
            'attributesValues' => $this->attributeValues,
            'selectedAttributes' => $this->selectedAttributes,
            'availableAttributes' => $this->availableAttributes,
            'attributesExtra' => $this->attributesExtra,
        ];
    }
}
