<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\Webshop\Entities\Product;
use Modules\Webshop\Entities\Variation;
use Modules\WebshopFrontend\Entities\ProductVariationList;


class ProductDealsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */


    public function toArray($request)
    {
        $product = $this->product;
        return [
            "id"=> $this->id,
            "sku"=> $this->sku,
            "name" => $product->name,
            "slug" => $product->slug,
            "in_stock" => $this->in_stock,
            "color_id"=> $this->color_id,
            "pack_id"=> $this->pack_id,
            "type_id"=> $this->type_id,
            "main_image"=> $this->main_image,
            "price_default"=> "".$this->price,
            "deals" => "".$this->deals,
            "discount_to" => strtotime($this->discount_to)

        ];
    }

}
