<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\Webshop\Entities\Product;
use Modules\Webshop\Entities\Variation;
use Modules\WebshopFrontend\Entities\ProductVariationList;


class ProductVariationDetailsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */


    public function toArray($request)
    {

        return [
            "id"=> $this->id,
            "sku"=> $this->sku,
            "color_id"=> $this->color_id,
            "pack_id"=> $this->pack_id,
            "type_id"=> $this->type_id,
            "main_image"=> $this->main_image,
            "images" => $this->images,
            "weight" => $this->weight,
            "info" => $this->info,
            "tags" => $this->tags,
            "in_stock" => $this->in_stock,
            "price"=> "".$this->getPrice(),
            "price_default"=> "".$this->price,
            "percent"=> (1 - ($this->getPrice()/$this->price))*100,
            "discount_to" => strtotime($this->discount_to),

        ];
    }

}
