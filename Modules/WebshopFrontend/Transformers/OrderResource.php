<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;


class OrderResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
            "phone" => $this->phone,
            "billing_name" => $this->billing_name,
            "billing_country" => $this->billing_country,
            "billing_zip" => $this->billing_zip,
            "billing_city" => $this->billing_city,
            "billing_address" => $this->billing_address,
            "vat_number" => $this->vat_number,
            "shipping_name" => $this->shipping_name,
            "shipping_country" => $this->shipping_country,
            "shipping_zip" => $this->shipping_zip,
            "shipping_city" => $this->shipping_city,
            "shipping_address" => $this->shipping_address,
            "currency_id" => $this->currency_id,
            "payment_name" => $this->payment->name,
            "shipment_name" => $this->shipment->name,
            "order_status_name" => $this->orderStatus->name,
            "net_value" => price_format($this->net_value, $this->currency_id),
            "gross_value" => price_format($this->gross_value, $this->currency_id),
            "tax_rate" => price_format($this->tax_rate, $this->currency_id),
            "order_total" => price_format($this->order_total, $this->currency_id),
            "items"=> OrderItemResource::collection($this->items)
        ];
    }
}
