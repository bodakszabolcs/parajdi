<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Webshop\Entities\Coupon;
use Modules\Webshop\Entities\ProductCategory;
use Modules\Webshop\Entities\ProductStock;
use Modules\Webshop\Entities\Stock;
use Modules\Webshop\Entities\VariationPrice;
use Modules\Webshop\Transformers\Product\ProductViewResource;


class VariationCollectionDetailsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    protected $coupon;

    public function toArray($request)
    {
        $price = VariationPrice::getVariationPrice($this->id,[],app('currency'), $this->product_id, false,null, null, $this->coupon);
        return [
            "id" => $this->id,
            "sku"=> $this->sku,
            "dimension" => $this->dimensions,
            "name"=>$this->product->name,
            "weight" => $this->weight,
            "images" => $this->attachments()->select('image_path')->get()->toArray(),
            "price" => $price,
            "price_default" => $this->price_gross,
            "sum_quantity" => $this->sum_quantity,
            "price_default_formatted" => price_format($this->price_gross,app('currency')),
            "price_formatted" => price_format($price, app('currency')),
            "attributes" => $this->variationAttribute,
            "stocks" => $this->getStocks()

        ];
    }

}
