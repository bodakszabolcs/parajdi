<?php

namespace Modules\WebshopFrontend\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::table('menu_items')->delete();

        \DB::table('menu_items')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'menu_id' => 1,
                    'parent_id' => NULL,
                    'order' => 1,
                    'title' => 'Home',
                    'link' => '/',
                    'created_at' => '2020-03-12 15:42:00',
                    'updated_at' => '2020-03-12 15:42:00',
                ),
            1 =>
                array (
                    'id' => 2,
                    'menu_id' => 1,
                    'parent_id' => NULL,
                    'order' => 2,
                    'title' => 'Shop',
                    'link' => '/shop',
                    'created_at' => '2020-03-12 15:42:00',
                    'updated_at' => '2020-03-12 15:42:00',
                ),
            2 =>
                array (
                    'id' => 3,
                    'menu_id' => 1,
                    'parent_id' => NULL,
                    'order' => 3,
                    'title' => 'Cart',
                    'link' => '/cart',
                    'created_at' => '2020-03-12 15:42:00',
                    'updated_at' => '2020-03-12 15:42:00',
                ),
            3 =>
                array (
                    'id' => 4,
                    'menu_id' => 1,
                    'parent_id' => NULL,
                    'order' => 4,
                    'title' => 'Contact',
                    'link' => '/contact',
                    'created_at' => '2020-03-12 15:42:00',
                    'updated_at' => '2020-03-12 15:42:00',
                ),
        ));

        $m = \Modules\Menu\Entities\Menu::find(1);
        $m->generateMenuJson();
    }
}
