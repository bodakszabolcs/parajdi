<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        DB::connection()->getPdo()->exec('DROP view IF exists product_variation_images;');
        DB::connection()->getPdo()->exec('DROP view IF exists product_variation_list;');
        DB::connection()->getPdo()->exec('DROP view IF exists product_prices;');
        DB::connection()->getPdo()->exec("CREATE view product_variation_images as (select `ws_products_attachments`.`variation_id` AS `variation_id`,concat('{ \"',`ws_products_attachments`.`variation_id`,'\": [\"',group_concat(`ws_products_attachments`.`image_path` separator '\",\"'),'\"]}') AS `image_json` from `ws_products_attachments` group by `ws_products_attachments`.`variation_id`);");
        DB::connection()->getPdo()->exec("CREATE VIEW `product_prices` AS
                (SELECT
                ws_product_variation_prices.variation_id,
                ws_product_variation_prices.id,
                ws_product_variation_prices.currency_id,
                ws_product_variation_prices.price_net,
                ws_product_variation_prices.discount_from,
                ws_product_variation_prices.discount_until,
                ws_product_variation_prices.price_discount_net,
                ws_product_variation_prices.price_discount_gross,
                ws_product_variation_prices.price_gross
                FROM
                ws_product_variation_prices
                LEFT JOIN ws_product_stocks ON ws_product_stocks.variation_id = ws_product_variation_prices.variation_id
                LEFT JOIN ws_product_variations ON ws_product_variation_prices.variation_id = ws_product_variations.id
                LEFT JOIN ws_products ON ws_product_variations.ws_product_id = ws_products.id
                WHERE
                ws_product_stocks.sum_quantity > 0 AND
                ws_product_variations.enabled = 1 AND
                ws_products.enabled = 1
                );");

        DB::connection()->getPdo()->exec('CREATE view product_variation_list as (
                SELECT
                ws_product_variation_attributes.*,
                ws_products.`name` AS `name`,
                ws_product_variations.ws_product_id,
                ws_products.slug AS slug,
                ws_products.description AS description,
                ws_products.tags AS tags,
                ws_products.attribute_set_id AS attribute_set_id,
                product_variation_images.image_json AS image_json,
                ws_product_variation_attributes.id AS attr_id,
                ws_products.categories_json AS categories_json,
                ws_products_categories.category_id AS category_id,
                ws_product_stocks.sum_quantity AS sum_quantity
                from (((((`ws_product_variation_attributes` left join `ws_product_variations` on((`ws_product_variation_attributes`.`product_variation_id` = `ws_product_variations`.`id`)))
                 left join `ws_products` on((`ws_product_variations`.`ws_product_id` = `ws_products`.`id`)))
                 left join `product_variation_images` on((`product_variation_images`.`variation_id` = `ws_product_variations`.`id`))) join `ws_products_categories` on((`ws_products_categories`.`ws_product_id` = `ws_products`.`id`))) left join `ws_product_stocks` on((`ws_product_stocks`.`variation_id` = `ws_product_variations`.`id`)))
                where ((`ws_product_variations`.`deleted_at` is null) and (`ws_products`.`deleted_at` is null) and (`ws_product_variations`.`enabled` = 1) and (`ws_product_stocks`.`sum_quantity` > 0) and (`ws_product_variations`.`is_default` <> 1 or `ws_product_variations`.`is_default` is NULL) and ws_products.enabled = 1)
                );');
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        DB::connection()->getPdo()->exec('DROP view IF exists product_variation_images;');
        DB::connection()->getPdo()->exec('DROP view IF exists product_variation_list;');
        DB::connection()->getPdo()->exec('DROP view IF exists product_prices;');*/
    }
}
