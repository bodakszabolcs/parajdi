<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/social-login/{provider}', 'App\Http\Controllers\Auth\LoginController@SocialSignup');

Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/','middleware' => ['auth:sanctum']], function () {
    Route::get('/checkout/index/auth', 'CheckoutController@index')->name('Checkout with auth');
    Route::get('/orders/my-orders', 'OrderController@getMyOrders')->name('Get my orders');
});
Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/','middleware' => []], function () {

    Route::get('/menu-category', 'WebshopFrontendController@getMenuCategory')->name('Frontend get menu category');
    Route::get('/get-slides', 'WebshopFrontendController@getSlides')->name('Frontend get slides');
    Route::get('/get-exclusive-products', 'WebshopFrontendController@exclusiveProducts')->name('Exclusive Products');
    Route::get('/get-deals', 'WebshopFrontendController@dealsOfDays')->name('Deals of Day');
    Route::get('/get-three-coll', 'WebshopFrontendController@threeCollProducts')->name('Three coll products');
    Route::get('/get-banners', 'WebshopFrontendController@getBanners')->name('Get Front banners');
    Route::post('/rate-product', 'WebshopFrontendController@RateProduct')->name('Frontend Rate Product');
    Route::put('/rate-useful', 'WebshopFrontendController@RateUseful')->name('Frontend rate useful');
    Route::post('/question-product', 'WebshopFrontendController@QuestionProduct')->name('Frontend question product');
    Route::put('/question-useful', 'WebshopFrontendController@QuestionUseful')->name('Frontend question useful');
    Route::get('/product/list/{slug?}', 'WebshopFrontendController@index')->name('Frontend product List');
    Route::get('/product/get-additional', 'WebshopFrontendController@getProductAdditional')->name('Frontend product List additional');
    Route::get('/product/details/{slug}', 'WebshopFrontendController@getProductDetails')->name('Frontend product by slug');
    Route::get('/product/related/{id}', 'WebshopFrontendController@getRelatedProducts')->name('Frontend get related products');

    Route::get('/get-currencies', 'WebshopFrontendController@getCurrencies')->name('Frontend get currencies');
    Route::get('/checkout/index', 'CheckoutController@index')->name('Checkout');
    Route::post('/checkout/save', 'CheckoutController@save')->name('Checkout save');
    Route::post('/cart/apply-coupon', 'CartController@applyCoupon')->name('Add coupon');
    Route::put('/cart/remove-coupon', 'CartController@removeCoupon')->name('Remove coupon');
    Route::put('/update-amount/{uuid}', 'CartController@updateAmount')->name('Update my cart');
    Route::get('/home', 'WebshopFrontendController@getHomeProducts')->name('Frontend home page products');



});

Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/cart','middleware' => []], function () {
    Route::post('/add-to-cart/{uuid?}', 'CartController@addToCart')->name('Product add to cart');
    Route::get('/my-cart/{uuid?}', 'CartController@getMyCart')->name('Get my cart');

    Route::delete('/remove/{uuid}/{id}', 'CartController@removeFromCart')->name('Remove product from cart');
    //Route::post('/coupon/{uuid}', 'CartController@applyCoupon')->name('Apply coupon');
    //Route::delete('/remove-coupon/{uuid}', 'CartController@removeCoupon')->name('Remove coupon');

});
