<?php

namespace Modules\WebshopFrontend\Providers;

use App\Providers\ModuleServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class WebshopFrontendServiceProvider extends ModuleServiceProvider
{
    protected $module = 'webshopfrontend';
    protected $directory = __DIR__;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->isRegisterViews = true;
        parent::boot();
    }
}
