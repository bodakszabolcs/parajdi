import ProductList from '../components/products/ProductList'
import Profile from '../components/profile/Profile'
import Shop from '../components/Shop'
import LoginWeb from '../components/profile/LoginWeb'
import RegistrationWeb from '../components/profile/RegistrationWeb'
import ForgetPasswordWeb from '../components/profile/ForgetPasswordWeb'
import Logout from '../../../../../../resources/js/components/Logout'
import ResetPasswordWeb from '../components/profile/ResetPasswordWeb'
import ProductDetails from '../components/product-details/ProductDetails'
import CheckoutProcess from '../components/cart/CheckoutProcess'

export default [
    {
        path: '/shop',
        name: 'Shop',
        component: Shop,
        meta: {
            title: 'ProductList'
        },
        children: [
            {
                path: ':category?',
                name: 'ProductList',
                component: ProductList
            }
        ]
    },
    {
        path: '/auth/twitter',
        component: {
            template: '<div class="auth-component"></div>'
        }
    },
    {
        path: '/auth/:provider/:callback?',
        component: {
            template: '<div class="auth-component"></div>'
        }
    },
    {
        path: '/product-details/:slug',
        name: 'ProductDetails',
        component: ProductDetails,
        meta: {
            title: 'Product Details'
        }
    },
    {
        path: '/checkout/:slug',
        component: CheckoutProcess,
        name: 'Checkout',
        meta: {
            title: 'CheckoutProcess'
        }
    },

    {
        path: '/profile',
        component: Profile,
        name: 'Profile',
        meta: {
            requiresAuth: true,
            title: 'Profile'
        }
    },
    {
        path: '/login',
        component: LoginWeb,
        name: 'Login',
        meta: {
            title: 'Login'
        }
    },
    {
        path: '/logout',
        component: Logout,
        name: 'Logout',
        meta: {
            title: 'Logout'
        }
    },
    {
        path: '/register',
        component: RegistrationWeb,
        name: 'Register',
        meta: {
            title: 'Register'
        }
    },
    {
        path: '/forget',
        component: ForgetPasswordWeb,
        name: 'Forget',
        meta: {
            title: 'Forget'
        }
    },
    {
        path: '/reset-password',
        component: ResetPasswordWeb,
        name: 'Reset',
        meta: {
            title: 'Reset password'
        }
    }
]
