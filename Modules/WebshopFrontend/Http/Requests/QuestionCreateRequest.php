<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'product_id' => 'required',
            'name' => 'required',
            'comment' => 'required',
            'title'=> 'required'

        ];
    }

    public function attributes()
    {
        return [
            'email' => __('Email'),
            'product_id' => __('Termék'),
            'name' => __('Név'),
            'comment' => __('Kérdés'),
            'title' => __('Cím'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
