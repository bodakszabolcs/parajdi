<?php

namespace Modules\WebshopFrontend\Http\Controllers\Base;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Modules\Order\Entities\Order;
use Modules\WebshopFrontend\Transformers\CheckoutResource;
use Modules\WebshopFrontend\Http\Requests\OrderCreateRequest;
use Modules\WebshopFrontend\Transformers\OrderResource;


class BaseOrderController extends Controller
{

    public function getMyOrders(Request $request){

        $orders = Order::where('user_id','=',Auth::user()->id)->orderBy('id','desc')->paginate(5)->withPath($request->path());
        return OrderResource::collection($orders)->additional();
    }
}
