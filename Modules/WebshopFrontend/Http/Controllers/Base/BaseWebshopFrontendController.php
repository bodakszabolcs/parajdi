<?php

namespace Modules\WebshopFrontend\Http\Controllers\Base;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Banner\Entities\Banner;
use Modules\Banner\Transformers\BannerListResource;
use Modules\ProductCategory\Transformers\ProductCategoryThreeResource;
use Modules\ProductCategory\Entities\ProductCategory;
use Modules\ProductVariation\Entities\ProductVariation;
use Modules\Slider\Entities\Slider;
use Modules\Slider\Transformers\SliderListResource;
use Modules\WebshopFrontend\Entities\Base\FrontProducts;
use Modules\WebshopFrontend\Transformers\ProductDealsResource;
use Modules\WebshopFrontend\Transformers\ProductListResource;
class BaseWebshopFrontendController extends Controller
{

    public function getSlides(Request $request){
        return SliderListResource::collection(Slider::orderBy('order')->get());
    }

    public function getMenuCategory(Request $request){
        return  ProductCategoryThreeResource::collection(ProductCategory::whereNull('parent_id')->get());
    }
    public function exclusiveProducts(Request $request){
        $prods = new FrontProducts();
        $news = $prods->ProductCollection()->orderBy('created_at','desc')->limit(8)->get();
        $bestSeller = $prods->ProductCollection()->leftJoin(DB::raw('(select count(*) as order_count,product_id from order_items group by product_id) as o_items'),'o_items.product_id','=','products.id')->select(DB::raw("products.*"))
            ->orderBy("order_count","desc")->limit(8)->get();
        $featured = $prods->ProductCollection()->orderBy('ist','desc')->limit(8)->get();
        return response()->json([
            'newArrival'=>  ProductListResource::collection($news),
            'bestSeller' => ProductListResource::collection($bestSeller),
            'featured' => ProductListResource::collection($featured),

        ]+$prods->additional(),200);

    }
    public function getBanners(Request $request){


        return response()->json([
                'right'=>  BannerListResource::collection(Banner::whereIn('id',[1,2])->get()),
                'big' => new BannerListResource(Banner::where('id','=',3)->first()),
                'bottom' => BannerListResource::collection(Banner::whereIn('id',[4,5,6])->get()),

            ],200);
    }
    public function threeCollProducts(Request $request){
        $prods = new FrontProducts();
        $news = $prods->ProductCollection()->orderBy('created_at','desc')->offset(8)->limit(9)->get();
        $topRated = $prods->ProductCollection()->orderBy('avg_rate','desc')->limit(9)->get();
        $onSale = $prods->ProductCollection()
            ->leftJoin(DB::raw("(select product_id,max(IF(discount_from <= '" .date("Y-m-d H:i:s")."' and discount_to >= '" .date("Y-m-d H:i:s")."',discount_price,0)) as deals from product_variations group by product_id) as onsale"),'onsale.product_id','=','products.id')
            ->where('deals','>',0)->limit(9)->get();
        return response()->json([
                'news'=>  ProductListResource::collection($news)->chunk(3),
                'topRated' => ProductListResource::collection($topRated)->chunk(3),
                'onSale' => ProductListResource::collection($onSale)->chunk(3),

            ]+$prods->additional(),200);
    }
    public function dealsOfDays(Request $request){
    $prods = new FrontProducts();
    $list = ProductVariation::where('in_stock','>',0)->where('active','=',1)
        ->select(DB::raw("product_variations.*,max(IF(discount_from <= '" .date("Y-m-d H:i:s")."' and discount_to >= '" .date("Y-m-d H:i:s")."',discount_price,0)) as deals"))
        ->having('deals','>',0)->groupBy('product_variations.id')->orderBy('discount_to','desc')->limit(4)->get();

    return ProductDealsResource::collection($list)->additional($prods->additional());
}





}
