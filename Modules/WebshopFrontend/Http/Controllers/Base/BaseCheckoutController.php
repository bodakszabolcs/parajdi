<?php

namespace Modules\WebshopFrontend\Http\Controllers\Base;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderDetails;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;
use Modules\WebshopFrontend\Entities\Cart;
use Modules\WebshopFrontend\Transformers\CartResource;
use Modules\WebshopFrontend\Transformers\CheckoutResource;
use Modules\WebshopFrontend\Http\Requests\OrderCreateRequest;


class BaseCheckoutController extends Controller
{
    protected $errorStatus = 422;

    public function index(Request $request){
        return new CheckoutResource([]);
    }

    public function save(OrderCreateRequest $request){
        $order = Cart::getMyCart($request, $request->input('uuid'), null);
        $userID = $request->input('user_id');
        $order->status = 1;
        $order->payment_method_id = $request->input('payment_method_id');
        $order->payment_status = 0;
        $order->user_id = $userID;
        $orderDetails = OrderDetails::where('order_id','=',$order->id)->first();
        if(is_null($orderDetails)) {
            $orderDetails = new OrderDetails();
        }
        $orderDetails->fill($request->all());

        $orderDetails->order_id = $order->id;
        $orderDetails->save();
        $order->shipping_price = $order->calculateShipping();
        $order->save();
        $this->orderTotal($order);

        if($userID){
            $shipping = UserShipping::where('user_id','=',$userID)->first();
            if(is_null($shipping)){
                $shipping = new UserShipping();
                $shipping->user_id = $userID;
            }
            $shipping->name = $request->input('shipping_name');
            $shipping->email = $request->input('shipping_email');
            $shipping->phone = $request->input('shipping_phone');
            $shipping->country_id = $request->input('shipping_country_id');
            $shipping->city_id = $request->input('shipping_city_id');
            $shipping->zip = $request->input('shipping_zip');
            $shipping->address = $request->input('shipping_address');
            $shipping->note = $request->input('shipping_note');
            $shipping->save();

            $billing = UserBilling::where('user_id','=',$userID)->first();
            if(is_null($billing)){
                $billing = new UserBilling();
                $billing->user_id = $userID;
            }
            $billing->name = $request->input('billing_name');
            $billing->email = $request->input('billing_email');
            $billing->phone = $request->input('billing_phone');
            $billing->country = $request->input('billing_country');
            $billing->city = $request->input('billing_city');
            $billing->zip = $request->input('billing_zip');
            $billing->address = $request->input('billing_address');
            $billing->vat_number = $request->input('vat_number');
            $billing->company_name = $request->input('billing_company_name');
            $billing->save();
        }

        return new CartResource($order);
    }
    public function orderTotal($order)
    {
        $total = 0;
        foreach ($order->items as $orderItem) {
            $orderItem->unit_price = $orderItem->variation->getPrice();
            $orderItem->save();
            $total +=  $orderItem->quantity *  $orderItem->unit_price;
        }
        if($order->coupons() && $order->coupons()->type!= 3){
            if($order->coupons()->type== 1){
                $order->discount = $order->coupons()->value;
                $total -=  $this->coupons()->value;
            }
            if($order->coupons()->type== 2){
                $order->discount =  ($total * ($order->coupons()->value/100));
                $total -= ($total * ($order->coupons()->value/100));
            }
        }
        $order->order_total = $total +$order->shipping_price;
        $order->save();
    }

}
