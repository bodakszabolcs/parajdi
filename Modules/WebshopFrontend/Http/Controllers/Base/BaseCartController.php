<?php

    namespace Modules\WebshopFrontend\Http\Controllers\Base;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller;
    use Modules\Coupon\Entities\Coupon;
    use Modules\WebshopFrontend\Entities\Cart;
    use Modules\WebshopFrontend\Entities\CartItem;
    use Modules\WebshopFrontend\Transformers\CartResource;
    class BaseCartController extends Controller
    {
        protected $errorStatus = 422;

        public function getMyCart(Request $request, $uuid = null, $userId = null)
        {

            return new CartResource(Cart::getMyCart($request, $uuid, $userId));
        }

        public function applyCoupon(Request $request)
        {
            try {
                $coupon = Coupon::where('code', '=', $request->input('code'))->where('date_from', '<=', date('Y-m-d H:i:s'))->where('date_to', '>=', date('Y-m-d H:i:s'))->firstOrFail();
                $cart = Cart::getMyCart($request, $request->input('uuid'), $request->input('uid'));
                if (!sizeof($cart->items)) {
                    throw new ModelNotFoundException();
                }
                $cart->coupon = $coupon->code;
                $cart->save();

                return new CartResource($cart);
            } catch (ModelNotFoundException $e) {
                return \response()->json(['errors' => [__('Invalid coupon code or not applicable')]], $this->errorStatus);
            }

        }

        public function removeCoupon(Request $request)
        {
            $cart = $this->getMyCart($request, $request->input('uuid'), $request->input('uid'));
            $cart->coupon = null;
            $cart->save();

            return new CartResource($cart);
        }

        public function addToCart(Request $request)
        {
            $cart = $this->getMyCart($request, $request->input('uuid'), $request->input('uid'));
            $cartProduct = $cart->items()->where('variation_id', '=', $request->input('variation_id'))->first();
            if ($cartProduct) {
                $all = $cartProduct->quantity + $request->input('quantity');
                if ($cartProduct->variation->in_stock < $all) {
                    return \response()->json(['errors' => [__('quantity is greater then available: ') . $cartProduct->variation->in_stock]], $this->errorStatus);
                }
                $cartProduct->quantity = $all;
                $cartProduct->save();

                return new CartResource($cart);
            }
            $cartItem = new CartItem();
            $cartItem->fill($request->all());
            $cartItem->order_id = $cart->id;
            $cartItem->save();

            return new CartResource($cart);
        }

        public function removeFromCart(Request $request, $uuid, $cartItemId)
        {
            $cart = $this->getMyCart($request, $uuid, $request->input('uid'));
            $cart->items()->where('id', '=', $cartItemId)->forceDelete();

            return new CartResource($cart);
        }

        public function updateAmount(Request $request, $uuid)
        {
            $cart = $this->getMyCart($request, $uuid, $request->input('uid'));
            try {
                $cartProduct = $cart->items()->where('id', '=', $request->input('id'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                return \response()->json(['errors' => [__('Product not found')]], $this->errorStatus);
            }
            $all = $request->input('quantity');
            if ($cartProduct->variation->in_stock < $all) {
                return \response()->json(['errors' => [__('quantity is greater then available: ') . $cartProduct->variation->in_stock]], $this->errorStatus);
            }
            $cartProduct->quantity = $all;
            $cartProduct->save();

            return new CartResource($cart);
        }
    }
