<?php

    namespace Modules\WebshopFrontend\Http\Controllers;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    use Modules\Category\Transformers\CategoryListResource;
    use Modules\Category\Transformers\CategoryTreeResource;
    use Modules\Color\Transformers\ColorListResource;
    use Modules\ProductCategory\Entities\ProductCategory;
    use Modules\ProductCategory\Transformers\ProductCategoryThreeResource;
    use Modules\ProductQuestion\Entities\ProductQuestion;
    use Modules\ProductRating\Entities\ProductRating;
    use Modules\WebshopFrontend\Entities\Base\FrontProducts;
    use Modules\WebshopFrontend\Http\Controllers\Base\BaseWebshopFrontendController;
    use Modules\WebshopFrontend\Http\Requests\QuestionCreateRequest;
    use Modules\WebshopFrontend\Http\Requests\RateCreateRequest;
    use Modules\WebshopFrontend\Transformers\ProductDetailsResource;
    use Modules\WebshopFrontend\Transformers\ProductListResource;
    class WebshopFrontendController extends BaseWebshopFrontendController
    {
        public function index(Request $request, $slug = null)
        {
            $prods = new FrontProducts();
            $list = $prods->ProductCollection();
            if ($slug) {
                // $list = $list->join('product_categories','product_categories.product_id','=','products.id')->where('slug','=',$slug);
            }
            if ($request->has('search')) {
                $search = explode(' ', $request->input('search'));
                $list = $list->where(function ($query) use ($search) {
                    foreach ($search as $s) {
                        $query = $query->where(function ($query) use ($s) {
                            $query = $query->where('name', 'LIKE', '%' . $s . '%');
                        });
                        $query = $query->orWhere(function ($query) use ($s) {
                            $query = $query->where('description', 'LIKE', '%' . $s . '%');
                        });
                    }
                });
            }
            $pagination = 12;
            $list = $list->paginate($pagination)->withPath($request->path());

            return ProductListResource::collection($list);
        }

        public function getProductAdditional(Request $request)
        {
            $prods = new FrontProducts();
            $breadcrumb = [];
            if ($request->has('category_id')) {
                $cat = ProductCategory::where('category_id', '=', $request->input('category_id', 0))->first();
                if ($cat->parent_id) {
                    $parent = ProductCategory::where('category_id', '=', $cat->parent_id)->first();
                    $breadcrumb[] = new CategoryListResource($parent);
                }
                $breadcrumb[] = new CategoryListResource($cat);
            }

            return response()->json([
                    'categoryThree' => ProductCategoryThreeResource::collection(ProductCategory::whereNull('parent_id')->get()),
                ] + $prods->additional());
        }

        public function RateProduct(RateCreateRequest $request)
        {
            try {
                $rate = new ProductRating();
                $rate->fill($request->all());
                $rate->save();

                return response()->json('OK', 200);
            } catch (\Exception $e) {
                return response()->json('Error', 422);
            }
        }

        public function RateUseful(Request $request)
        {
            try {
                $rate = ProductRating::where('id', '=', $request->input('id'))->firstOrFail();
                if ($request->input('type')) {
                    $rate->useful++;
                } else {
                    $rate->not_useful++;
                }
                $rate->save();

                return response()->json('OK', 200);
            } catch (ModelNotFoundException $e) {
                return response()->json('Error', 422);
            }
        }

        public function QuestionUseful(Request $request)
        {
            try {
                $question = ProductQuestion::where('id', '=', $request->input('id'))->firstOrFail();
                if ($request->input('type')) {
                    $question->useful++;
                } else {
                    $question->not_useful++;
                }
                $question->save();

                return response()->json('OK', 200);
            } catch (ModelNotFoundException $e) {
                return response()->json('Error', 422);
            }
        }

        public function QuestionProduct(QuestionCreateRequest $request)
        {
            try {
                $question = new ProductQuestion();
                $question->fill($request->all());
                $question->save();

                return response()->json('OK', 200);
            } catch (\Exception $e) {
                return response()->json('Error', 422);
            }
        }

        public function getProductDetails(Request $request, $slug)
        {
            try {
                $prod = new FrontProducts();
                $prod = $prod->ProductCollection()->where('slug', '=', $slug)->firstOrFail();
                return new ProductDetailsResource($prod);
            } catch (ModelNotFoundException $e) {
                return response()->json("Model not found", 404);
            }
        }
        public function getRelatedProducts(Request $request, $id)
        {
            try {
                $category = \DB::table('product_categories')->where('product_id','=',$id)->get()->pluck('category_id','category_id')->toArray();
                $prod = new FrontProducts();
                $list = $prod->ProductCollection()->join('product_categories','product_categories.product_id','=','products.id')
                ->whereIn('category_id',$category)->groupBy('products.id')->where('products.id','<>',$id)->inRandomOrder()->limit(8)->get();

                return ProductListResource::collection($list);
            } catch (ModelNotFoundException $e) {
                return response()->json("Model not found", 404);
            }

        }
    }
